-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_perpus2
CREATE DATABASE IF NOT EXISTS `db_perpus2` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_perpus2`;

-- Dumping structure for table db_perpus2.tb_access
CREATE TABLE IF NOT EXISTS `tb_access` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_access: ~11 rows (approximately)
/*!40000 ALTER TABLE `tb_access` DISABLE KEYS */;
INSERT INTO `tb_access` (`access_id`, `menu_id`, `role_id`) VALUES
	(21, 2, 1),
	(22, 11, 1),
	(23, 11, 2),
	(24, 12, 1),
	(25, 13, 1),
	(26, 13, 2),
	(27, 14, 1),
	(28, 14, 2),
	(29, 15, 1),
	(30, 15, 2),
	(31, 16, 1);
/*!40000 ALTER TABLE `tb_access` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_booking
CREATE TABLE IF NOT EXISTS `tb_booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_noId` int(11) NOT NULL,
  `booking_user` int(11) NOT NULL,
  `booking_buku` int(11) NOT NULL,
  `booking_jumlah` int(11) NOT NULL,
  `booking_waktu` datetime NOT NULL,
  `booking_pengembalian` date NOT NULL,
  `booking_expired` datetime NOT NULL,
  `booking_accept` int(11) NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_booking: ~10 rows (approximately)
/*!40000 ALTER TABLE `tb_booking` DISABLE KEYS */;
INSERT INTO `tb_booking` (`booking_id`, `booking_noId`, `booking_user`, `booking_buku`, `booking_jumlah`, `booking_waktu`, `booking_pengembalian`, `booking_expired`, `booking_accept`) VALUES
	(5, 297382, 23, 1, 1, '2020-08-11 00:14:22', '2020-09-29', '2020-08-11 02:14:22', 1),
	(6, 590667, 23, 1, 1, '2020-08-11 01:22:30', '2020-09-30', '2020-08-11 03:22:30', 2),
	(7, 854130, 23, 6, 1, '2020-08-11 03:05:24', '2020-09-29', '2020-08-11 05:05:24', 2),
	(8, 854130, 23, 3, 1, '2020-08-11 03:05:24', '2020-09-29', '2020-08-11 05:05:24', 2),
	(9, 854130, 23, 5, 1, '2020-08-11 03:05:24', '2020-09-29', '2020-08-11 05:05:24', 2),
	(10, 972729, 23, 1, 1, '2020-08-11 14:59:08', '2020-09-29', '2020-08-11 16:59:08', 2),
	(11, 499597, 23, 4, 1, '2020-08-12 15:50:33', '2020-09-29', '2020-08-12 17:50:33', 2),
	(12, 73766, 23, 5, 1, '2020-08-14 08:43:38', '2020-09-29', '2020-08-14 10:43:38', 1),
	(13, 484275, 23, 5, 1, '2020-08-14 08:47:03', '2020-09-09', '2020-08-14 10:47:03', 1),
	(14, 484275, 23, 1, 1, '2020-08-14 08:47:03', '2020-09-09', '2020-08-14 10:47:03', 2);
/*!40000 ALTER TABLE `tb_booking` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_buku
CREATE TABLE IF NOT EXISTS `tb_buku` (
  `buku_id` int(11) NOT NULL AUTO_INCREMENT,
  `buku_author` varchar(100) NOT NULL,
  `buku_badanKoorporasi` varchar(100) NOT NULL,
  `buku_seminar` varchar(100) NOT NULL,
  `buku_judulSeragam` varchar(100) NOT NULL,
  `buku_judul` varchar(255) NOT NULL,
  `buku_penulis` varchar(255) NOT NULL,
  `buku_edisi` varchar(100) NOT NULL,
  `buku_kota` varchar(100) NOT NULL,
  `buku_penerbit` varchar(100) NOT NULL,
  `buku_tahunTerbit` varchar(100) NOT NULL,
  `buku_kolasi` varchar(100) NOT NULL,
  `buku_seri` varchar(100) NOT NULL,
  `buku_judulAsli` varchar(100) NOT NULL,
  `buku_catatan` varchar(100) NOT NULL,
  `buku_blibiografi` varchar(100) NOT NULL,
  `buku_indeks` varchar(100) NOT NULL,
  `buku_isbn` varchar(100) NOT NULL,
  `buku_noSKU` varchar(255) NOT NULL,
  `buku_stok` int(11) NOT NULL DEFAULT '0',
  `buku_rak` varchar(100) NOT NULL,
  `buku_sumber1` varchar(100) NOT NULL,
  `buku_keterangan` varchar(100) NOT NULL,
  `buku_tahunAnggaran` varchar(100) NOT NULL,
  `buku_foto` varchar(100) NOT NULL,
  PRIMARY KEY (`buku_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_buku: ~20 rows (approximately)
/*!40000 ALTER TABLE `tb_buku` DISABLE KEYS */;
INSERT INTO `tb_buku` (`buku_id`, `buku_author`, `buku_badanKoorporasi`, `buku_seminar`, `buku_judulSeragam`, `buku_judul`, `buku_penulis`, `buku_edisi`, `buku_kota`, `buku_penerbit`, `buku_tahunTerbit`, `buku_kolasi`, `buku_seri`, `buku_judulAsli`, `buku_catatan`, `buku_blibiografi`, `buku_indeks`, `buku_isbn`, `buku_noSKU`, `buku_stok`, `buku_rak`, `buku_sumber1`, `buku_keterangan`, `buku_tahunAnggaran`, `buku_foto`) VALUES
	(1, 'Abdul Jabbar Yoesoef, 1961-', '-', '-', '-', 'Perpustakaan Indonesia', 'Abdul Jabbar Yoesoef ; editor, Rayendra Lumbantoruan', 'Cet. 2', 'Jakarta', 'Elex Media Komputindo', '2011', 'xviii, 267 hlm. ; 23 cm.', '-', '-', '-', 'Bibliografi : hlm. 259-267', '-', '978-979-27-9624-7', '001.3', 7, 'Layanan Umum', 'PERPUM PUWAKARTA', '-', '2019', 'default.jpg'),
	(2, 'Ardial', '-', '-', '-', 'Perkataan Para Filsafat', 'Ardial ; Editor Restu Damayanti', 'Cet. 1', 'Jakarta', 'Bumi Aksara', '2014', 'xii, 538 hlm. ; 23 cm.', '-', '-', '-', 'Bibliografi : hlm. 477 - 488', '-', '978-602-217-261-1', '100.4', 2, 'Layanan Filsafat', '-', '-', '2020', 'default.jpg'),
	(3, 'Burhan Ashshofa', '-', '-', '-', 'Pemahaman Al-Qur`an dan Sunnah', 'Burhan Ashshofa; editor : -', 'cet. 3', 'Jakarta', 'Cahaya Sunnah', '2001', 'ix, 187 hlm. ; 21 cm', '-', '-', '-', 'Bibliografi : hlm. 185-187', '-', '979-518-662-0', '200.5', 4, 'Layanan Agama', '-', '-', '2018', 'default.jpg'),
	(4, 'Sutrisno Hadi', '-', '-', '-', 'Metodologi Sosial', 'Sutrisno Hadi; Editor : ', '-', 'Yogyakarta', 'Andi Yogyakarta', '2004', 'xii, 96 hlm. ; 16 x 23 cm.', '-', '-', '-', 'Bibliografi : hlm. 95', '-', '979-731-448-0', '300.3', 3, 'Layanan Ilmu Sosial', '-', '-', '2015', 'default.jpg'),
	(5, 'Singarimbun', '-', '-', '-', 'Bahasa Indonesia - Arab', 'Singarimbun; Editor : Masri', 'cet. 18', 'Jakarta', 'Pustaka LP3ES indonesia', '2006', 'x, 336 hlm. ; 21 cm.', '-', '-', '-', 'Bibliografi : hlm. 326-327', 'indeks', '9799-8015-47-9', '400.6', 3, 'Layanan Bahasa', '-', '-', '2017', 'default.jpg'),
	(6, '-', 'Badan Peneliotian Dan Pengembangan Departemen Dalam negeri Dan Otonomi Daerah Republik Indonesia', '-', '-', 'Dunia itu Bulat', 'Faqih', '-', '-', 'Duniaku', '2000', 'x,285 hlm. ; 21 cm.', '-', '-', '-', '-', '-', '-', '500.9', 2, 'Layanan Ilmu Murni', '-', '-', '2018', 'default.jpg'),
	(7, 'John W.Creswell', '-', '-', '-', 'Pemrograman Berbasis Web mengguanak PHP', 'John W.Creswell ; Editor :Syaifuddin Zuhri ;penerjemah :Achmad Fawaid', 'cet. 3', 'Yogyakarta', 'Pustaka Pelajar', '2013', 'xxv, 383 hlm. ; 23 cm.', '-', 'Research Desain', '-', 'Bibliografi : hlm. 359-378', 'indeks', '978-602-8764-84-1', '600.3', 1, 'Layanan Ilmu Terapan', '-', '-', '2015', 'default.jpg'),
	(8, 'Pangestu Subagyo', '-', '-', '-', 'Bahaya Covid-19', 'Pangestu Subagyo; Editor :', 'cet. 13', 'Yogyakarta', 'BPFE-Yogyakarta', '2000', 'viii, 314 hlm. ; 21 cm.', '-', '-', '-', 'Bibliografi : hlm. 313-314', '-', '979-503-173-2', '700.3', 1, 'Layanan Kesehatan dan Olahraga', '-', '-', '2019', 'default.jpg'),
	(9, 'Veven SP. Wardhana ', '-', '-', '-', 'Kapitalisme Televisi Dan Stategi Budaya Massa', 'Veven SP. Wardhana ; Editor : Utjuk R.E', 'cet. 1', 'Yogyakarta', 'Pustaka Pelajar', '1997', 'x, 304 hlm. ; 21 cm.', '-', '-', '-', '-', '-', '979-8581-78-4', '800.2', 1, 'Layanan Kesusastraan', '-', '-', '2020', 'default.jpg'),
	(10, 'R. Satmoko', '-', '-', '-', 'Proklamasi 1945', 'R. Satmoko ; Editor : Kasdullah', 'cet. 1', '-', 'Pedar kindy', '2016', 'xii, 98 hlm. ; 14 x 21 cm.', '-', '-', '-', 'Bibliografi : hlm. 98', '-', '978-602-6421-07-4', '900.3', 1, 'Layanan Sejarah dan Geografi', '-', '-', '2016', 'default.jpg'),
	(11, 'Abdul Jabbar Yoesoef, 1961-', '-', '-', '-', 'Pentingnya Sekolah', 'Abdul Jabbar Yoesoef ; editor, Rayendra Lumbantoruan', 'Cet. 2', 'Jakarta', 'Elex Media Komputindo', '2011', 'xviii, 267 hlm. ; 23 cm.', '-', '-', '-', 'Bibliografi : hlm. 259-267', '-', '978-979-27-9624-7', '009.3', 3, 'Layanan Umum', 'PERPUM PUWAKARTA', '-', '2019', 'default.jpg'),
	(12, 'Ardial', '-', '-', '-', 'Ahli Filsafat', 'Ardial ; Editor Restu Damayanti', 'Cet. 1', 'Jakarta', 'Bumi Aksara', '2014', 'xii, 538 hlm. ; 23 cm.', '-', '-', '-', 'Bibliografi : hlm. 477 - 488', '-', '978-602-217-261-1', '130.4', 1, 'Layanan Filsafat', '-', '-', '2020', 'default.jpg'),
	(13, 'Burhan Ashshofa', '-', '-', '-', 'Pemahaman Salafushalih', 'Burhan Ashshofa; editor : -', 'cet. 3', 'Jakarta', 'Cahaya Sunnah', '2001', 'ix, 187 hlm. ; 21 cm', '-', '-', '-', 'Bibliografi : hlm. 185-187', '-', '979-518-662-0', '260.5', 3, 'Layanan Agama', '-', '-', '2018', 'default.jpg'),
	(14, 'Sutrisno Hadi', '-', '-', '-', 'Sosialisai Industri', 'Sutrisno Hadi; Editor : ', '-', 'Yogyakarta', 'Andi Yogyakarta', '2004', 'xii, 96 hlm. ; 16 x 23 cm.', '-', '-', '-', 'Bibliografi : hlm. 95', '-', '979-731-448-0', '330.3', 2, 'Layanan Ilmu Sosial', '-', '-', '2015', 'default.jpg'),
	(15, 'Singarimbun', '-', '-', '-', 'Bahasa Indonesia - Inggris', 'Singarimbun; Editor : Masri', 'cet. 18', 'Jakarta', 'Pustaka LP3ES indonesia', '2006', 'x, 336 hlm. ; 21 cm.', '-', '-', '-', 'Bibliografi : hlm. 326-327', 'indeks', '9799-8015-47-9', '490.6', 1, 'Layanan Bahasa', '-', '-', '2017', 'default.jpg'),
	(16, '-', 'Badan Peneliotian Dan Pengembangan Departemen Dalam negeri Dan Otonomi Daerah Republik Indonesia', '-', '-', 'Dunia - Bulan - Matahari', 'Adit', '-', '-', 'Duniaku', '2000', 'x,285 hlm. ; 21 cm.', '-', '-', '-', '-', '-', '-', '530.9', 1, 'Layanan Ilmu Murni', '-', '-', '2018', 'default.jpg'),
	(17, 'John W.Creswell', '-', '-', '-', 'HTML - PHP - MYSQL', 'John W.Creswell ; Editor :Syaifuddin Zuhri ;penerjemah :Achmad Fawaid', 'cet. 3', 'Yogyakarta', 'Pustaka Pelajar', '2013', 'xxv, 383 hlm. ; 23 cm.', '-', 'Research Desain', '-', 'Bibliografi : hlm. 359-378', 'indeks', '978-602-8764-84-1', '650.3', 1, 'Layanan Ilmu Terapan', '-', '-', '2015', 'default.jpg'),
	(18, 'Pangestu Subagyo', '-', '-', '-', 'Bahaya Virus Bagi Kehidupan', 'Pangestu Subagyo; Editor :', 'cet. 13', 'Yogyakarta', 'BPFE-Yogyakarta', '2000', 'viii, 314 hlm. ; 21 cm.', '-', '-', '-', 'Bibliografi : hlm. 313-314', '-', '979-503-173-2', '740.3', 1, 'Layanan Kesehatan dan Olahraga', '-', '-', '2019', 'default.jpg'),
	(19, 'Veven SP. Wardhana ', '-', '-', '-', 'Budaya Indonesia', 'Veven SP. Wardhana ; Editor : Utjuk R.E', 'cet. 1', 'Yogyakarta', 'Pustaka Pelajar', '1997', 'x, 304 hlm. ; 21 cm.', '-', '-', '-', '-', '-', '979-8581-78-4', '810.2', 1, 'Layanan Kesusastraan', '-', '-', '2020', 'default.jpg'),
	(20, 'R. Satmoko', '-', '-', '-', 'Perjuangan NKRI', 'R. Satmoko ; Editor : Kasdullah', 'cet. 1', '-', 'Pedar kindy', '2016', 'xii, 98 hlm. ; 14 x 21 cm.', '-', '-', '-', 'Bibliografi : hlm. 98', '-', '978-602-6421-07-4', '909.3', 1, 'Layanan Sejarah dan Geografi', '-', '-', '2016', 'default.jpg');
/*!40000 ALTER TABLE `tb_buku` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_cart
CREATE TABLE IF NOT EXISTS `tb_cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_noId` int(11) NOT NULL,
  `cart_user` int(11) NOT NULL,
  `cart_buku` int(11) NOT NULL,
  `cart_jumlah` int(11) NOT NULL,
  `cart_hari` varchar(255) NOT NULL,
  `cart_tanggal` date NOT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_cart: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_cart` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_denda
CREATE TABLE IF NOT EXISTS `tb_denda` (
  `denda_id` int(11) NOT NULL AUTO_INCREMENT,
  `denda_harga` int(11) NOT NULL,
  PRIMARY KEY (`denda_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_denda: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_denda` DISABLE KEYS */;
INSERT INTO `tb_denda` (`denda_id`, `denda_harga`) VALUES
	(1, 10000);
/*!40000 ALTER TABLE `tb_denda` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_identitas_orangtua
CREATE TABLE IF NOT EXISTS `tb_identitas_orangtua` (
  `orangtua_id` int(11) NOT NULL AUTO_INCREMENT,
  `orangtua_user` varchar(255) NOT NULL DEFAULT '',
  `orangtua_nama` varchar(255) NOT NULL DEFAULT '',
  `orangtua_alamat` longtext NOT NULL,
  `orangtua_tempatLahir` varchar(255) NOT NULL DEFAULT '',
  `orangtua_tanggalLahir` date NOT NULL,
  `orangtua_noHP` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`orangtua_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_identitas_orangtua: ~5 rows (approximately)
/*!40000 ALTER TABLE `tb_identitas_orangtua` DISABLE KEYS */;
INSERT INTO `tb_identitas_orangtua` (`orangtua_id`, `orangtua_user`, `orangtua_nama`, `orangtua_alamat`, `orangtua_tempatLahir`, `orangtua_tanggalLahir`, `orangtua_noHP`) VALUES
	(4, '22', 'Agus', '', 'Bekasi', '1965-07-13', '081465546224'),
	(5, '23', 'User', '', 'User', '1967-07-06', '0812754512544'),
	(6, '24', 'Bambang', '', 'Bekasi', '1976-07-20', '081254124512'),
	(11, '29', 'Ruhastuti', '', 'Sragen', '1971-09-29', '081212499837'),
	(12, '30', 'Ruhastuti', '', 'Sragen', '1971-09-29', '081212499837');
/*!40000 ALTER TABLE `tb_identitas_orangtua` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_klasifikasi
CREATE TABLE IF NOT EXISTS `tb_klasifikasi` (
  `pekerjaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pekerjaan` varchar(50) NOT NULL,
  PRIMARY KEY (`pekerjaan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_klasifikasi: ~4 rows (approximately)
/*!40000 ALTER TABLE `tb_klasifikasi` DISABLE KEYS */;
INSERT INTO `tb_klasifikasi` (`pekerjaan_id`, `pekerjaan`) VALUES
	(1, 'Pelajar'),
	(2, 'Mahasiswa'),
	(3, 'PNS'),
	(4, 'Karyawan Swasta'),
	(5, 'Guru'),
	(6, 'Programmer');
/*!40000 ALTER TABLE `tb_klasifikasi` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_log
CREATE TABLE IF NOT EXISTS `tb_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_user` int(11) NOT NULL,
  `log_tanggal` date NOT NULL,
  `log_time` time NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_log: ~195 rows (approximately)
/*!40000 ALTER TABLE `tb_log` DISABLE KEYS */;
INSERT INTO `tb_log` (`log_id`, `log_user`, `log_tanggal`, `log_time`) VALUES
	(1, 22, '2020-07-25', '00:34:07'),
	(2, 22, '2020-07-25', '00:56:54'),
	(3, 22, '2020-07-25', '00:57:17'),
	(4, 22, '2020-07-25', '03:51:02'),
	(5, 22, '2020-07-25', '03:59:00'),
	(6, 22, '2020-07-25', '09:08:12'),
	(7, 22, '2020-07-25', '09:57:00'),
	(8, 22, '2020-07-25', '13:07:10'),
	(9, 22, '2020-07-25', '14:33:37'),
	(10, 22, '2020-07-26', '01:38:57'),
	(11, 22, '2020-07-26', '03:31:12'),
	(12, 22, '2020-07-26', '06:16:18'),
	(13, 22, '2020-07-26', '09:21:09'),
	(14, 23, '2020-07-26', '10:18:44'),
	(15, 23, '2020-07-26', '10:23:40'),
	(16, 23, '2020-07-26', '10:26:51'),
	(17, 23, '2020-07-26', '10:30:50'),
	(18, 23, '2020-07-26', '10:31:16'),
	(19, 22, '2020-07-26', '10:36:50'),
	(20, 23, '2020-07-26', '10:38:05'),
	(21, 22, '2020-07-26', '12:59:26'),
	(22, 23, '2020-07-26', '13:04:41'),
	(23, 22, '2020-07-26', '13:34:31'),
	(24, 22, '2020-07-26', '14:31:47'),
	(25, 22, '2020-07-26', '21:35:54'),
	(26, 22, '2020-07-26', '21:45:01'),
	(27, 22, '2020-07-26', '23:30:54'),
	(28, 23, '2020-07-26', '23:38:24'),
	(29, 24, '2020-07-26', '23:39:34'),
	(30, 23, '2020-07-26', '23:44:23'),
	(31, 22, '2020-07-27', '04:28:28'),
	(32, 23, '2020-07-27', '06:42:22'),
	(33, 24, '2020-07-27', '06:55:54'),
	(34, 23, '2020-07-27', '07:00:35'),
	(35, 22, '2020-07-27', '07:01:13'),
	(36, 23, '2020-07-27', '07:13:24'),
	(37, 24, '2020-07-27', '07:14:18'),
	(38, 23, '2020-07-27', '08:13:14'),
	(39, 22, '2020-07-27', '08:14:00'),
	(40, 23, '2020-07-27', '08:26:22'),
	(41, 22, '2020-07-27', '08:29:34'),
	(42, 23, '2020-07-27', '09:08:43'),
	(43, 24, '2020-07-27', '09:12:21'),
	(44, 23, '2020-07-27', '09:31:41'),
	(45, 24, '2020-07-27', '10:47:34'),
	(46, 23, '2020-07-27', '10:53:07'),
	(47, 24, '2020-07-27', '11:16:29'),
	(48, 23, '2020-07-27', '11:55:14'),
	(49, 24, '2020-07-27', '11:56:47'),
	(50, 24, '2020-07-27', '12:26:52'),
	(51, 23, '2020-07-27', '12:27:20'),
	(52, 24, '2020-07-27', '12:29:13'),
	(53, 23, '2020-07-27', '14:04:08'),
	(54, 23, '2020-07-27', '14:35:45'),
	(55, 24, '2020-07-27', '14:43:24'),
	(56, 23, '2020-07-27', '14:58:30'),
	(57, 24, '2020-07-27', '14:59:55'),
	(58, 23, '2020-07-27', '15:03:56'),
	(59, 24, '2020-07-27', '15:05:36'),
	(60, 22, '2020-07-27', '20:13:01'),
	(61, 24, '2020-07-27', '20:24:28'),
	(62, 22, '2020-07-27', '20:35:22'),
	(63, 22, '2020-07-27', '20:46:03'),
	(64, 22, '2020-07-27', '21:30:13'),
	(65, 24, '2020-07-27', '22:16:19'),
	(66, 22, '2020-07-27', '23:17:41'),
	(67, 23, '2020-07-28', '09:05:27'),
	(68, 24, '2020-07-28', '09:08:05'),
	(69, 24, '2020-07-28', '14:40:44'),
	(70, 22, '2020-07-28', '14:55:59'),
	(71, 22, '2020-07-28', '15:00:31'),
	(72, 23, '2020-07-28', '19:46:55'),
	(73, 24, '2020-07-28', '19:57:45'),
	(74, 23, '2020-07-28', '20:13:31'),
	(75, 24, '2020-07-28', '20:24:17'),
	(76, 23, '2020-07-28', '20:44:02'),
	(77, 24, '2020-07-28', '21:02:29'),
	(78, 23, '2020-07-28', '21:38:04'),
	(79, 24, '2020-07-28', '21:59:41'),
	(80, 23, '2020-07-28', '22:13:08'),
	(81, 23, '2020-07-28', '22:42:30'),
	(82, 23, '2020-07-29', '00:42:41'),
	(83, 24, '2020-07-29', '00:48:21'),
	(84, 23, '2020-07-29', '01:40:02'),
	(85, 24, '2020-07-29', '05:28:31'),
	(86, 23, '2020-07-29', '05:30:23'),
	(87, 24, '2020-07-29', '05:34:24'),
	(88, 23, '2020-07-29', '05:41:31'),
	(89, 24, '2020-07-29', '11:54:31'),
	(90, 23, '2020-07-29', '12:02:27'),
	(91, 23, '2020-07-29', '13:21:21'),
	(92, 23, '2020-07-30', '11:28:00'),
	(93, 22, '2020-07-30', '12:11:06'),
	(94, 23, '2020-07-30', '12:31:22'),
	(95, 23, '2020-07-30', '12:31:56'),
	(96, 24, '2020-07-30', '13:01:34'),
	(97, 23, '2020-07-30', '13:09:09'),
	(98, 24, '2020-07-30', '13:13:56'),
	(99, 23, '2020-07-30', '14:07:46'),
	(100, 24, '2020-07-30', '14:57:23'),
	(101, 24, '2020-07-30', '21:36:27'),
	(102, 22, '2020-07-30', '21:37:11'),
	(103, 23, '2020-07-30', '21:39:21'),
	(104, 24, '2020-07-31', '12:36:07'),
	(105, 23, '2020-08-01', '18:52:25'),
	(106, 24, '2020-08-01', '18:56:03'),
	(107, 23, '2020-08-01', '19:03:45'),
	(108, 24, '2020-08-01', '19:05:41'),
	(109, 23, '2020-08-01', '19:07:42'),
	(110, 24, '2020-08-01', '19:08:43'),
	(111, 22, '2020-08-01', '19:32:02'),
	(112, 23, '2020-08-01', '19:38:45'),
	(113, 22, '2020-08-01', '19:40:38'),
	(114, 23, '2020-08-01', '19:59:49'),
	(115, 22, '2020-08-01', '20:06:05'),
	(116, 24, '2020-08-01', '20:43:29'),
	(117, 24, '2020-08-01', '20:59:33'),
	(118, 24, '2020-08-01', '21:21:14'),
	(119, 23, '2020-08-02', '00:07:09'),
	(120, 28, '2020-08-02', '03:00:36'),
	(121, 24, '2020-08-02', '03:00:47'),
	(122, 24, '2020-08-02', '03:17:33'),
	(123, 22, '2020-08-02', '03:20:13'),
	(124, 22, '2020-08-02', '17:27:39'),
	(125, 30, '2020-08-02', '23:08:55'),
	(126, 24, '2020-08-03', '00:23:58'),
	(127, 22, '2020-08-03', '00:24:30'),
	(128, 22, '2020-08-03', '06:46:51'),
	(129, 24, '2020-08-03', '07:32:42'),
	(130, 23, '2020-08-03', '07:33:12'),
	(131, 23, '2020-08-05', '06:56:29'),
	(132, 23, '2020-08-05', '19:52:23'),
	(133, 24, '2020-08-05', '20:10:54'),
	(134, 23, '2020-08-05', '20:14:55'),
	(135, 23, '2020-08-07', '19:31:33'),
	(136, 23, '2020-08-07', '23:45:34'),
	(137, 23, '2020-08-08', '11:00:37'),
	(138, 23, '2020-08-08', '13:35:13'),
	(139, 30, '2020-08-08', '16:52:21'),
	(140, 24, '2020-08-08', '16:58:41'),
	(141, 23, '2020-08-08', '17:02:54'),
	(142, 22, '2020-08-08', '21:35:42'),
	(143, 22, '2020-08-08', '22:26:29'),
	(144, 23, '2020-08-08', '22:48:48'),
	(145, 22, '2020-08-08', '23:06:21'),
	(146, 23, '2020-08-08', '23:09:30'),
	(147, 24, '2020-08-09', '12:02:11'),
	(148, 23, '2020-08-09', '12:05:14'),
	(149, 22, '2020-08-09', '12:12:51'),
	(150, 24, '2020-08-09', '15:15:26'),
	(151, 23, '2020-08-10', '09:37:47'),
	(152, 24, '2020-08-10', '13:48:18'),
	(153, 22, '2020-08-10', '14:07:06'),
	(154, 22, '2020-08-10', '16:03:28'),
	(155, 22, '2020-08-10', '18:54:02'),
	(156, 22, '2020-08-10', '19:34:21'),
	(157, 22, '2020-08-10', '19:47:53'),
	(158, 23, '2020-08-10', '23:07:54'),
	(159, 23, '2020-08-10', '23:10:05'),
	(160, 23, '2020-08-11', '00:03:43'),
	(161, 24, '2020-08-11', '00:12:19'),
	(162, 23, '2020-08-11', '00:13:59'),
	(163, 24, '2020-08-11', '00:14:39'),
	(164, 23, '2020-08-11', '00:56:34'),
	(165, 23, '2020-08-11', '03:04:25'),
	(166, 23, '2020-08-11', '14:13:55'),
	(167, 22, '2020-08-11', '14:15:56'),
	(168, 23, '2020-08-11', '14:51:00'),
	(169, 22, '2020-08-11', '14:51:41'),
	(170, 23, '2020-08-11', '14:58:37'),
	(171, 22, '2020-08-11', '15:02:29'),
	(172, 24, '2020-08-11', '15:27:30'),
	(173, 22, '2020-08-12', '03:25:41'),
	(174, 22, '2020-08-12', '10:10:47'),
	(175, 23, '2020-08-12', '15:48:51'),
	(176, 23, '2020-08-12', '19:01:52'),
	(177, 22, '2020-08-13', '01:06:40'),
	(178, 31, '2020-08-13', '14:54:29'),
	(179, 22, '2020-08-13', '16:21:42'),
	(180, 22, '2020-08-14', '08:18:13'),
	(181, 23, '2020-08-14', '08:43:22'),
	(182, 24, '2020-08-14', '08:44:25'),
	(183, 23, '2020-08-14', '08:46:18'),
	(184, 24, '2020-08-14', '08:47:31'),
	(185, 22, '2020-08-14', '09:37:24'),
	(186, 22, '2020-08-14', '10:54:25'),
	(187, 23, '2020-08-14', '12:05:56'),
	(188, 24, '2020-08-14', '12:14:05'),
	(189, 22, '2020-08-14', '12:16:23'),
	(190, 24, '2020-08-14', '12:16:41'),
	(191, 22, '2020-08-14', '12:17:32'),
	(192, 22, '2020-08-14', '21:13:57'),
	(193, 23, '2020-08-15', '08:55:28'),
	(194, 23, '2020-08-15', '08:55:28'),
	(195, 23, '2020-08-15', '10:01:38'),
	(196, 23, '2020-08-15', '10:01:38'),
	(197, 22, '2020-08-15', '10:09:22'),
	(198, 22, '2020-08-15', '10:37:11'),
	(199, 22, '2020-08-15', '13:15:13'),
	(200, 22, '2020-08-15', '20:16:36'),
	(201, 23, '2020-08-15', '21:30:20'),
	(202, 23, '2020-08-15', '21:33:26'),
	(203, 22, '2020-08-15', '22:38:47');
/*!40000 ALTER TABLE `tb_log` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_masukan
CREATE TABLE IF NOT EXISTS `tb_masukan` (
  `masukan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subjek` varchar(50) NOT NULL,
  `pesan` text NOT NULL,
  `waktu` datetime NOT NULL,
  PRIMARY KEY (`masukan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_masukan: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_masukan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_masukan` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_menu
CREATE TABLE IF NOT EXISTS `tb_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_judul` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_menu: ~7 rows (approximately)
/*!40000 ALTER TABLE `tb_menu` DISABLE KEYS */;
INSERT INTO `tb_menu` (`menu_id`, `menu_judul`) VALUES
	(2, 'Menu'),
	(11, 'Anggota'),
	(12, 'Petugas'),
	(13, 'User'),
	(14, 'Buku'),
	(15, 'Manajemen Buku'),
	(16, 'Website');
/*!40000 ALTER TABLE `tb_menu` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_peminjaman
CREATE TABLE IF NOT EXISTS `tb_peminjaman` (
  `peminjaman_id` int(11) NOT NULL AUTO_INCREMENT,
  `peminjaman_noId` varchar(255) NOT NULL DEFAULT '0',
  `peminjaman_user` varchar(255) NOT NULL,
  `peminjaman_buku` int(20) NOT NULL,
  `peminjaman_jumlah` int(11) NOT NULL,
  `peminjaman_dari` date NOT NULL,
  `peminjaman_sampai` date NOT NULL,
  `peminjaman_kembali` date NOT NULL,
  `peminjaman_denda` varchar(255) NOT NULL,
  `peminjaman_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`peminjaman_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_peminjaman: ~7 rows (approximately)
/*!40000 ALTER TABLE `tb_peminjaman` DISABLE KEYS */;
INSERT INTO `tb_peminjaman` (`peminjaman_id`, `peminjaman_noId`, `peminjaman_user`, `peminjaman_buku`, `peminjaman_jumlah`, `peminjaman_dari`, `peminjaman_sampai`, `peminjaman_kembali`, `peminjaman_denda`, `peminjaman_status`) VALUES
	(1, '297382', '23', 1, 1, '2020-08-11', '2020-09-29', '2020-09-30', '10000', 2),
	(2, '73766', '23', 5, 1, '2020-08-14', '2020-09-29', '2020-09-28', '0', 2),
	(3, '484275', '23', 5, 1, '2020-08-14', '2020-09-09', '0000-00-00', '0', 3),
	(4, '395953', '24', 5, 2, '2020-08-20', '2020-09-20', '2020-09-20', '0', 2),
	(5, '259419', '30', 13, 3, '2020-08-29', '2020-09-29', '0000-00-00', '0', 3),
	(6, '495815', '29', 6, 2, '2020-08-28', '2020-09-29', '2020-09-30', '10000', 2),
	(7, '830647', '29', 5, 1, '2020-08-23', '2020-09-01', '0000-00-00', '0', 3),
	(8, '735010', '22', 5, 1, '2020-08-23', '2020-08-27', '0000-00-00', '0', 1);
/*!40000 ALTER TABLE `tb_peminjaman` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_pengunjung
CREATE TABLE IF NOT EXISTS `tb_pengunjung` (
  `pengunjung_id` int(11) NOT NULL AUTO_INCREMENT,
  `browser` text NOT NULL,
  `alamat_ip` varchar(50) DEFAULT NULL,
  `waktu` date DEFAULT NULL,
  PRIMARY KEY (`pengunjung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_pengunjung: ~21 rows (approximately)
/*!40000 ALTER TABLE `tb_pengunjung` DISABLE KEYS */;
INSERT INTO `tb_pengunjung` (`pengunjung_id`, `browser`, `alamat_ip`, `waktu`) VALUES
	(9, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(10, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(11, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(12, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(13, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(14, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(15, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(16, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(17, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(18, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(19, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(20, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(21, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(22, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(23, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(24, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(25, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(26, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(27, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(28, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(29, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(30, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(31, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(32, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(33, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(34, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(35, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(36, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(37, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(38, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(39, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(40, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(41, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(42, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(43, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(44, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(45, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(46, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(48, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(49, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(50, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(51, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(52, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(53, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(54, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(55, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(56, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(57, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(58, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15'),
	(59, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', '::1', '2020-08-15');
/*!40000 ALTER TABLE `tb_pengunjung` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_pertanyaan_keamanan
CREATE TABLE IF NOT EXISTS `tb_pertanyaan_keamanan` (
  `pertanyaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan_user` int(11) NOT NULL,
  `pertanyaan` varchar(255) NOT NULL,
  `pertanyaan_jawaban` varchar(255) NOT NULL,
  PRIMARY KEY (`pertanyaan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_pertanyaan_keamanan: ~5 rows (approximately)
/*!40000 ALTER TABLE `tb_pertanyaan_keamanan` DISABLE KEYS */;
INSERT INTO `tb_pertanyaan_keamanan` (`pertanyaan_id`, `pertanyaan_user`, `pertanyaan`, `pertanyaan_jawaban`) VALUES
	(4, 22, 'Nama sekolah SD anda adalah?', 'Gandasari'),
	(5, 23, 'Siapa nama peliharaan anda?', 'use'),
	(6, 24, 'Siapa nama peliharaan anda?', 'admin'),
	(11, 29, 'Nama sekolah SD anda adalah?', 'cinyosog2'),
	(12, 30, 'Siapa nama kakek anda?', 'Slamet');
/*!40000 ALTER TABLE `tb_pertanyaan_keamanan` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_sub
CREATE TABLE IF NOT EXISTS `tb_sub` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `sub_judul` varchar(255) NOT NULL,
  `sub_link` varchar(255) NOT NULL,
  `sub_icon` varchar(255) NOT NULL,
  `sub_status` int(11) NOT NULL,
  PRIMARY KEY (`sub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_sub: ~11 rows (approximately)
/*!40000 ALTER TABLE `tb_sub` DISABLE KEYS */;
INSERT INTO `tb_sub` (`sub_id`, `menu_id`, `sub_judul`, `sub_link`, `sub_icon`, `sub_status`) VALUES
	(1, 2, 'Menu', 'menu', 'fas fa-fw fa-bars', 1),
	(2, 2, 'Sub Menu', 'subMenu', 'fas fa-fw fa-ellipsis-v', 1),
	(3, 13, 'Log User', 'logUser', 'fas fa-fw fa-sign-in-alt', 1),
	(4, 11, 'Data Anggota', 'dataAnggota', 'fas fa-fw fa-users', 1),
	(6, 12, 'Data Petugas', 'dataPetugas', 'fas fa-fw fa-user', 1),
	(7, 13, 'Data User', 'dataUser', 'fas fa-fw fa-users', 2),
	(8, 14, 'Data Buku', 'dataBuku', 'fas fa-fw fa-book', 1),
	(9, 14, 'Katalog Buku', 'katalogBuku', 'fas fa-fw fa-bookmark', 1),
	(10, 15, 'Data Peminjaman Buku', 'peminjamanBuku', 'fas fa-fw fa-book-reader', 1),
	(11, 15, 'Data Booking', 'dataBooking', 'fas fa-fw fa-comments-dollar', 1),
	(12, 16, 'Website Settings', 'websiteSettings', 'fas fa-fw fa-info-circle', 1);
/*!40000 ALTER TABLE `tb_sub` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_user
CREATE TABLE IF NOT EXISTS `tb_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_noId` varchar(20) NOT NULL,
  `user_nama` varchar(255) NOT NULL,
  `user_jk` enum('L','P') NOT NULL,
  `user_tempatLahir` varchar(20) NOT NULL,
  `user_tanggalLahir` date NOT NULL,
  `user_klasifikasi` int(11) NOT NULL DEFAULT '0',
  `user_alamat` longtext NOT NULL,
  `user_ktp` varchar(255) NOT NULL,
  `user_foto` varchar(255) NOT NULL,
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_role` int(11) NOT NULL,
  `user_noHP` varchar(225) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_qr` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_klasifikasi` (`user_klasifikasi`),
  CONSTRAINT `FK_tb_user_tb_klasifikasi` FOREIGN KEY (`user_klasifikasi`) REFERENCES `tb_klasifikasi` (`pekerjaan_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_user: ~5 rows (approximately)
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` (`user_id`, `user_noId`, `user_nama`, `user_jk`, `user_tempatLahir`, `user_tanggalLahir`, `user_klasifikasi`, `user_alamat`, `user_ktp`, `user_foto`, `user_username`, `user_password`, `user_role`, `user_noHP`, `user_email`, `user_qr`) VALUES
	(22, 'A-000', 'Faqih Pratama Muhti', 'L', '', '1995-01-12', 1, '', '33290441570004', 'Faqih_pM.jpg', 'suadmin', '$2y$10$p4o8RX5Qf5KpiMdalNMzQOiPs6bH98C1g3juc5SBYwlRdssDVNF1y', 1, '', 'faqihpratamamuhti@gmail.com', '875693.png'),
	(23, '0001', 'Faqih', 'L', 'Jakarta', '1995-01-12', 2, '', '3325641241242', 'Faqih_pM.jpg', 'user', '$2y$10$XVvkxa422L8dP/tFltUf3e7MDAdC0CcEqZmso3XLZLtoBe2K/Y1RW', 3, '081212499837', 'si18.faqihmuhti@mhs.ubpkarawang.ac.id', '0001.png'),
	(24, 'P-000', 'Muhti', 'L', 'Bekasi', '1995-01-12', 3, '', '332654521541', 'Faqih_pM.jpg', 'admin', '$2y$10$EvegfYYp7/FlLCzyPeY1DO3xG0CxpY5Wu4ddivr6iTznD3cgMfGai', 2, '081212121212', 'admin@mail.com', 'P-000.png'),
	(29, '0002', 'Fitrya', 'P', 'Cileungsi', '2013-09-29', 4, '', '0123456789', 'Fitrya.png', 'fitrya', '$2y$10$dQmnxEpOzO1pWImdgrdigOWGh0nXfs4CwFKGeQp.jC89r/18lcaFa', 3, '083879511581', 'fitrya@gmail.com', '0002.png'),
	(30, '0003', 'Fadli', 'L', 'Jakarta', '1995-01-12', 5, '', '987654321', 'Fadli.jpeg', 'f1__m', '$2y$10$8Hf2bQXWTr7Imq4M1hpFleqAJv/Q2ev.ZIJNi5P8Jn/OEmL6et50W', 3, '082121991992', 'fadlie@gmail.com', '0003.png');
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;

-- Dumping structure for table db_perpus2.tb_website
CREATE TABLE IF NOT EXISTS `tb_website` (
  `website_id` int(11) NOT NULL AUTO_INCREMENT,
  `website_jum` text NOT NULL,
  `website_subjum` text NOT NULL,
  `website_gbrjum` varchar(255) NOT NULL,
  `website_tentang` text NOT NULL,
  `website_kontak` tinytext NOT NULL,
  `website_email` varchar(50) NOT NULL,
  `website_alamat` varchar(50) NOT NULL,
  `website_wa` varchar(255) NOT NULL,
  PRIMARY KEY (`website_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_perpus2.tb_website: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_website` DISABLE KEYS */;
INSERT INTO `tb_website` (`website_id`, `website_jum`, `website_subjum`, `website_gbrjum`, `website_tentang`, `website_kontak`, `website_email`, `website_alamat`, `website_wa`) VALUES
	(1, 'Selamat datang di Perpustakaan Daerah Karawang', 'Gudangnya ilmu pengetahuan', 'Karawang.png', '<p><strong>Perpustakan Daerah Kabupaten Karawang</strong>&nbsp;bisa menjadi penggerak terciptanya budaya baca, tulis dan budaya menghargai bahan bacaan oleh masyarakat di daerah tersebut.</p>\r\n\r\n<p><strong>Perpustakan Daerah Kabupaten Karawang</strong> akan terus meningkatkan mutu pelayanannya agar dapat menarik minat pengunjung untuk datang. Selain itu, akan terus berinovasi demi terciptanya minat baca bagi masyarakat Kabupaten Karawang.</p>\r\n', 'Assalamu`alaikum Warahmatullahi Wabarakatuh', 'info@perpusnas.ac.id', 'Jl. Jendral A. Yani No. 10 Karawang Barat', '081212499837');
/*!40000 ALTER TABLE `tb_website` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
