-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 08 Agu 2020 pada 22.43
-- Versi server: 5.7.24
-- Versi PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_perpus2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_access`
--

CREATE TABLE `tb_access` (
  `access_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_access`
--

INSERT INTO `tb_access` (`access_id`, `menu_id`, `role_id`) VALUES
(21, 2, 1),
(24, 12, 1),
(25, 13, 1),
(26, 13, 2),
(27, 14, 1),
(28, 14, 2),
(29, 15, 1),
(30, 15, 2),
(31, 16, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_booking`
--

CREATE TABLE `tb_booking` (
  `booking_id` int(11) NOT NULL,
  `booking_noId` int(11) NOT NULL,
  `booking_user` int(11) NOT NULL,
  `booking_buku` int(11) NOT NULL,
  `booking_jumlah` int(11) NOT NULL,
  `booking_waktu` datetime NOT NULL,
  `booking_pengembalian` date NOT NULL,
  `booking_expired` datetime NOT NULL,
  `booking_accept` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_booking`
--

INSERT INTO `tb_booking` (`booking_id`, `booking_noId`, `booking_user`, `booking_buku`, `booking_jumlah`, `booking_waktu`, `booking_pengembalian`, `booking_expired`, `booking_accept`) VALUES
(130, 908352, 23, 2, 2, '2020-07-30 14:40:20', '2020-08-30', '2020-07-30 14:51:00', 2),
(131, 908352, 23, 3, 3, '2020-07-30 14:40:20', '2020-08-30', '2020-07-30 14:50:34', 2),
(133, 108863, 23, 2, 2, '2020-08-01 18:55:05', '2020-08-03', '2020-08-01 20:55:05', 1),
(134, 108863, 23, 3, 3, '2020-08-01 18:55:05', '2020-08-03', '2020-08-01 20:55:05', 1),
(140, 959011, 30, 3, 1, '2020-08-08 16:57:40', '2020-09-12', '2020-08-08 18:57:40', 2),
(141, 856203, 23, 7, 1, '2020-08-08 21:03:51', '2020-09-29', '2020-08-08 23:03:51', 1),
(142, 856203, 23, 6, 1, '2020-08-08 21:03:51', '2020-09-29', '2020-08-08 23:03:51', 1),
(143, 856203, 23, 5, 1, '2020-08-08 21:03:51', '2020-09-29', '2020-08-08 23:03:51', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_buku`
--

CREATE TABLE `tb_buku` (
  `buku_id` int(11) NOT NULL,
  `buku_author` varchar(100) NOT NULL,
  `buku_badanKoorporasi` varchar(100) NOT NULL,
  `buku_seminar` varchar(100) NOT NULL,
  `buku_judulSeragam` varchar(100) NOT NULL,
  `buku_judul` varchar(255) NOT NULL,
  `buku_penulis` varchar(255) NOT NULL,
  `buku_edisi` varchar(100) NOT NULL,
  `buku_kota` varchar(100) NOT NULL,
  `buku_penerbit` varchar(100) NOT NULL,
  `buku_tahunTerbit` varchar(100) NOT NULL,
  `buku_kolasi` varchar(100) NOT NULL,
  `buku_seri` varchar(100) NOT NULL,
  `buku_judulAsli` varchar(100) NOT NULL,
  `buku_catatan` varchar(100) NOT NULL,
  `buku_blibiografi` varchar(100) NOT NULL,
  `buku_indeks` varchar(100) NOT NULL,
  `buku_isbn` varchar(100) NOT NULL,
  `buku_noSKU` varchar(255) NOT NULL,
  `buku_stok` int(11) NOT NULL DEFAULT '0',
  `buku_rak` varchar(100) NOT NULL,
  `buku_sumber1` varchar(100) NOT NULL,
  `buku_keterangan` varchar(100) NOT NULL,
  `buku_tahunAnggaran` varchar(100) NOT NULL,
  `buku_foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_buku`
--

INSERT INTO `tb_buku` (`buku_id`, `buku_author`, `buku_badanKoorporasi`, `buku_seminar`, `buku_judulSeragam`, `buku_judul`, `buku_penulis`, `buku_edisi`, `buku_kota`, `buku_penerbit`, `buku_tahunTerbit`, `buku_kolasi`, `buku_seri`, `buku_judulAsli`, `buku_catatan`, `buku_blibiografi`, `buku_indeks`, `buku_isbn`, `buku_noSKU`, `buku_stok`, `buku_rak`, `buku_sumber1`, `buku_keterangan`, `buku_tahunAnggaran`, `buku_foto`) VALUES
(2, 'Sanjaya', 'ghg', 'gfhgf', 'hjbn', 'xcvxc', 'fgcv', 'bnnbv', 'hjhj', 'bvnvbfgtt', '2017', 'kjlkjl', 'kjljkl', ',.,m.', ',m.,m', 'ghfghg', 'bnbvhg', 'fghgfh', '5451254', 1, 'kkjgjk', 'sdjfdkjgkj', 'ldjfkgmnn', '2020', 'default.jpg'),
(3, 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', '2016', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', '4545452184', 4, 'dcvcxv', 'dcvcxv', 'dcvcxv', '2020', 'default.jpg'),
(4, 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', '2017', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', '5451212154', 0, 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', '2020', 'default.jpg'),
(5, 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', '2019', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', '85745454', 4, 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', '2020', 'default.jpg'),
(6, 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', '2018', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', '544545411', 6, 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', '2020', 'default.jpg'),
(7, 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', '2018', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', '6956121', 4, 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', '2020', 'default.jpg'),
(8, 'Sanjaya', 'ghg', 'gfhgf', 'hjbn', ',.,.fafa', 'fgcv', 'bnnbv', 'hjhj', 'bvnvbfgtt', '2017', 'kjlkjl', 'kjljkl', ',.,.fafa', ',m.,m', 'ghfghg', 'bnbvhg', 'fghgfh', '094438', 1, 'kkjgjk', 'sdjfdkjgkj', 'ldjfkgmnn', '2020', 'default.jpg'),
(9, 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'wkwk', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', '2016', 'dcvcxv', 'dcvcxv', 'wkwk', 'dcvcxv', 'dcvcxv', 'dcvcxv', 'dcvcxv', '735120997', 4, 'dcvcxv', 'dcvcxv', 'dcvcxv', '2020', 'default.jpg'),
(10, 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'iuwtw', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', '2017', 'uhgujhjhn', 'uhgujhjhn', 'iuwtw', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', '8457', 3, 'uhgujhjhn', 'uhgujhjhn', 'uhgujhjhn', '2020', 'default.jpg'),
(11, 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'sisufs83ja', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', '2019', 'lkblnklkl', 'lkblnklkl', 'sisufs83ja', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', '834735', 5, 'lkblnklkl', 'lkblnklkl', 'lkblnklkl', '2020', 'default.jpg'),
(12, 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', '2018', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', '9545780', 7, 'vmcbnvmn', 'vmcbnvmn', 'vmcbnvmn', '2020', 'default.jpg'),
(13, 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'asdfiaga', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', '2018', 'knbjknjkjkn', 'knbjknjkjkn', 'asdfiaga', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', '835435', 5, 'knbjknjkjkn', 'knbjknjkjkn', 'knbjknjkjkn', '2020', 'default.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_cart`
--

CREATE TABLE `tb_cart` (
  `cart_id` int(11) NOT NULL,
  `cart_noId` int(11) NOT NULL,
  `cart_user` int(11) NOT NULL,
  `cart_buku` int(11) NOT NULL,
  `cart_jumlah` int(11) NOT NULL,
  `cart_hari` varchar(255) NOT NULL,
  `cart_tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_cart`
--

INSERT INTO `tb_cart` (`cart_id`, `cart_noId`, `cart_user`, `cart_buku`, `cart_jumlah`, `cart_hari`, `cart_tanggal`) VALUES
(7, 25432, 23, 3, 1, 'Sabtu', '2020-08-08'),
(8, 4557, 23, 7, 1, 'Sabtu', '2020-08-08'),
(9, 24013, 23, 6, 1, 'Sabtu', '2020-08-08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_denda`
--

CREATE TABLE `tb_denda` (
  `denda_id` int(11) NOT NULL,
  `denda_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_denda`
--

INSERT INTO `tb_denda` (`denda_id`, `denda_harga`) VALUES
(1, 20000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_identitas_orangtua`
--

CREATE TABLE `tb_identitas_orangtua` (
  `orangtua_id` int(11) NOT NULL,
  `orangtua_user` varchar(255) NOT NULL DEFAULT '',
  `orangtua_nama` varchar(255) NOT NULL DEFAULT '',
  `orangtua_tempatLahir` varchar(255) NOT NULL DEFAULT '',
  `orangtua_tanggalLahir` date NOT NULL,
  `orangtua_noHP` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_identitas_orangtua`
--

INSERT INTO `tb_identitas_orangtua` (`orangtua_id`, `orangtua_user`, `orangtua_nama`, `orangtua_tempatLahir`, `orangtua_tanggalLahir`, `orangtua_noHP`) VALUES
(4, '22', 'Agus', 'Bekasi', '1965-07-13', '081465546224'),
(5, '23', 'User', 'User', '1967-07-06', '0812754512544'),
(6, '24', 'Bambang', 'Bekasi', '1976-07-20', '081254124512'),
(7, '25', 'Ruhastuti', 'Sragen', '1971-09-29', '081212499837'),
(8, '26', 'Ruhastuti', 'Sragen', '0071-09-29', '081212499837'),
(9, '27', 'Ruhastuti', 'Sragen', '1971-09-29', '081212499837'),
(10, '28', 'Ruhastuti', 'Sragen', '1971-09-29', '08112499837'),
(11, '29', 'Ruhastuti', 'Sragen', '1971-09-29', '081212499837'),
(12, '30', 'Ruhastuti', 'Sragen', '1971-09-29', '081212499837');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_log`
--

CREATE TABLE `tb_log` (
  `log_id` int(11) NOT NULL,
  `log_user` int(11) NOT NULL,
  `log_tanggal` date NOT NULL,
  `log_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_log`
--

INSERT INTO `tb_log` (`log_id`, `log_user`, `log_tanggal`, `log_time`) VALUES
(1, 22, '2020-07-25', '00:34:07'),
(2, 22, '2020-07-25', '00:56:54'),
(3, 22, '2020-07-25', '00:57:17'),
(4, 22, '2020-07-25', '03:51:02'),
(5, 22, '2020-07-25', '03:59:00'),
(6, 22, '2020-07-25', '09:08:12'),
(7, 22, '2020-07-25', '09:57:00'),
(8, 22, '2020-07-25', '13:07:10'),
(9, 22, '2020-07-25', '14:33:37'),
(10, 22, '2020-07-26', '01:38:57'),
(11, 22, '2020-07-26', '03:31:12'),
(12, 22, '2020-07-26', '06:16:18'),
(13, 22, '2020-07-26', '09:21:09'),
(14, 23, '2020-07-26', '10:18:44'),
(15, 23, '2020-07-26', '10:23:40'),
(16, 23, '2020-07-26', '10:26:51'),
(17, 23, '2020-07-26', '10:30:50'),
(18, 23, '2020-07-26', '10:31:16'),
(19, 22, '2020-07-26', '10:36:50'),
(20, 23, '2020-07-26', '10:38:05'),
(21, 22, '2020-07-26', '12:59:26'),
(22, 23, '2020-07-26', '13:04:41'),
(23, 22, '2020-07-26', '13:34:31'),
(24, 22, '2020-07-26', '14:31:47'),
(25, 22, '2020-07-26', '21:35:54'),
(26, 22, '2020-07-26', '21:45:01'),
(27, 22, '2020-07-26', '23:30:54'),
(28, 23, '2020-07-26', '23:38:24'),
(29, 24, '2020-07-26', '23:39:34'),
(30, 23, '2020-07-26', '23:44:23'),
(31, 22, '2020-07-27', '04:28:28'),
(32, 23, '2020-07-27', '06:42:22'),
(33, 24, '2020-07-27', '06:55:54'),
(34, 23, '2020-07-27', '07:00:35'),
(35, 22, '2020-07-27', '07:01:13'),
(36, 23, '2020-07-27', '07:13:24'),
(37, 24, '2020-07-27', '07:14:18'),
(38, 23, '2020-07-27', '08:13:14'),
(39, 22, '2020-07-27', '08:14:00'),
(40, 23, '2020-07-27', '08:26:22'),
(41, 22, '2020-07-27', '08:29:34'),
(42, 23, '2020-07-27', '09:08:43'),
(43, 24, '2020-07-27', '09:12:21'),
(44, 23, '2020-07-27', '09:31:41'),
(45, 24, '2020-07-27', '10:47:34'),
(46, 23, '2020-07-27', '10:53:07'),
(47, 24, '2020-07-27', '11:16:29'),
(48, 23, '2020-07-27', '11:55:14'),
(49, 24, '2020-07-27', '11:56:47'),
(50, 24, '2020-07-27', '12:26:52'),
(51, 23, '2020-07-27', '12:27:20'),
(52, 24, '2020-07-27', '12:29:13'),
(53, 23, '2020-07-27', '14:04:08'),
(54, 23, '2020-07-27', '14:35:45'),
(55, 24, '2020-07-27', '14:43:24'),
(56, 23, '2020-07-27', '14:58:30'),
(57, 24, '2020-07-27', '14:59:55'),
(58, 23, '2020-07-27', '15:03:56'),
(59, 24, '2020-07-27', '15:05:36'),
(60, 22, '2020-07-27', '20:13:01'),
(61, 24, '2020-07-27', '20:24:28'),
(62, 22, '2020-07-27', '20:35:22'),
(63, 22, '2020-07-27', '20:46:03'),
(64, 22, '2020-07-27', '21:30:13'),
(65, 24, '2020-07-27', '22:16:19'),
(66, 22, '2020-07-27', '23:17:41'),
(67, 23, '2020-07-28', '09:05:27'),
(68, 24, '2020-07-28', '09:08:05'),
(69, 24, '2020-07-28', '14:40:44'),
(70, 22, '2020-07-28', '14:55:59'),
(71, 22, '2020-07-28', '15:00:31'),
(72, 23, '2020-07-28', '19:46:55'),
(73, 24, '2020-07-28', '19:57:45'),
(74, 23, '2020-07-28', '20:13:31'),
(75, 24, '2020-07-28', '20:24:17'),
(76, 23, '2020-07-28', '20:44:02'),
(77, 24, '2020-07-28', '21:02:29'),
(78, 23, '2020-07-28', '21:38:04'),
(79, 24, '2020-07-28', '21:59:41'),
(80, 23, '2020-07-28', '22:13:08'),
(81, 23, '2020-07-28', '22:42:30'),
(82, 23, '2020-07-29', '00:42:41'),
(83, 24, '2020-07-29', '00:48:21'),
(84, 23, '2020-07-29', '01:40:02'),
(85, 24, '2020-07-29', '05:28:31'),
(86, 23, '2020-07-29', '05:30:23'),
(87, 24, '2020-07-29', '05:34:24'),
(88, 23, '2020-07-29', '05:41:31'),
(89, 24, '2020-07-29', '11:54:31'),
(90, 23, '2020-07-29', '12:02:27'),
(91, 23, '2020-07-29', '13:21:21'),
(92, 23, '2020-07-30', '11:28:00'),
(93, 22, '2020-07-30', '12:11:06'),
(94, 23, '2020-07-30', '12:31:22'),
(95, 23, '2020-07-30', '12:31:56'),
(96, 24, '2020-07-30', '13:01:34'),
(97, 23, '2020-07-30', '13:09:09'),
(98, 24, '2020-07-30', '13:13:56'),
(99, 23, '2020-07-30', '14:07:46'),
(100, 24, '2020-07-30', '14:57:23'),
(101, 24, '2020-07-30', '21:36:27'),
(102, 22, '2020-07-30', '21:37:11'),
(103, 23, '2020-07-30', '21:39:21'),
(104, 24, '2020-07-31', '12:36:07'),
(105, 23, '2020-08-01', '18:52:25'),
(106, 24, '2020-08-01', '18:56:03'),
(107, 23, '2020-08-01', '19:03:45'),
(108, 24, '2020-08-01', '19:05:41'),
(109, 23, '2020-08-01', '19:07:42'),
(110, 24, '2020-08-01', '19:08:43'),
(111, 22, '2020-08-01', '19:32:02'),
(112, 23, '2020-08-01', '19:38:45'),
(113, 22, '2020-08-01', '19:40:38'),
(114, 23, '2020-08-01', '19:59:49'),
(115, 22, '2020-08-01', '20:06:05'),
(116, 24, '2020-08-01', '20:43:29'),
(117, 24, '2020-08-01', '20:59:33'),
(118, 24, '2020-08-01', '21:21:14'),
(119, 23, '2020-08-02', '00:07:09'),
(120, 28, '2020-08-02', '03:00:36'),
(121, 24, '2020-08-02', '03:00:47'),
(122, 24, '2020-08-02', '03:17:33'),
(123, 22, '2020-08-02', '03:20:13'),
(124, 22, '2020-08-02', '17:27:39'),
(125, 30, '2020-08-02', '23:08:55'),
(126, 24, '2020-08-03', '00:23:58'),
(127, 22, '2020-08-03', '00:24:30'),
(128, 22, '2020-08-03', '06:46:51'),
(129, 24, '2020-08-03', '07:32:42'),
(130, 23, '2020-08-03', '07:33:12'),
(131, 23, '2020-08-05', '06:56:29'),
(132, 23, '2020-08-05', '19:52:23'),
(133, 24, '2020-08-05', '20:10:54'),
(134, 23, '2020-08-05', '20:14:55'),
(135, 23, '2020-08-07', '19:31:33'),
(136, 23, '2020-08-07', '23:45:34'),
(137, 23, '2020-08-08', '11:00:37'),
(138, 23, '2020-08-08', '13:35:13'),
(139, 30, '2020-08-08', '16:52:21'),
(140, 24, '2020-08-08', '16:58:41'),
(141, 23, '2020-08-08', '17:02:54'),
(142, 22, '2020-08-08', '21:35:42'),
(143, 22, '2020-08-08', '22:26:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

CREATE TABLE `tb_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_judul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu`
--

INSERT INTO `tb_menu` (`menu_id`, `menu_judul`) VALUES
(2, 'Menu'),
(12, 'Petugas'),
(13, 'User'),
(14, 'Buku'),
(15, 'Manajemen Buku'),
(16, 'Website');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_peminjaman`
--

CREATE TABLE `tb_peminjaman` (
  `peminjaman_id` int(11) NOT NULL,
  `peminjaman_noId` varchar(255) NOT NULL DEFAULT '0',
  `peminjaman_user` varchar(255) NOT NULL,
  `peminjaman_buku` int(20) NOT NULL,
  `peminjaman_jumlah` int(11) NOT NULL,
  `peminjaman_dari` date NOT NULL,
  `peminjaman_sampai` date NOT NULL,
  `peminjaman_kembali` date NOT NULL,
  `peminjaman_denda` varchar(255) NOT NULL,
  `peminjaman_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_peminjaman`
--

INSERT INTO `tb_peminjaman` (`peminjaman_id`, `peminjaman_noId`, `peminjaman_user`, `peminjaman_buku`, `peminjaman_jumlah`, `peminjaman_dari`, `peminjaman_sampai`, `peminjaman_kembali`, `peminjaman_denda`, `peminjaman_status`) VALUES
(24, '993159', '23', 3, 2, '2020-07-07', '2020-07-27', '2020-07-28', '20000', 2),
(26, '425744', '23', 3, 3, '2020-07-27', '2020-07-27', '2020-07-27', '0', 2),
(27, '173986', '23', 2, 2, '2020-07-27', '2020-01-12', '2020-07-27', '3940000', 2),
(28, '234142', '23', 6, 2, '2020-07-27', '2020-07-28', '2020-07-27', '20000', 2),
(29, '373033', '23', 7, 1, '2020-07-27', '2020-07-29', '2020-07-28', '20000', 2),
(30, '951516', '23', 2, 1, '2020-07-27', '2020-08-29', '2020-08-29', '0', 2),
(31, '272618', '23', 2, 1, '2020-07-28', '2020-08-29', '2020-08-27', '40000', 2),
(33, '446532', '23', 3, 1, '2020-07-28', '2020-07-30', '2020-07-29', '20000', 2),
(34, '382026', '23', 3, 2, '2020-07-28', '2020-09-29', '2020-07-28', '1260000', 2),
(35, '614983', '23', 2, 40, '2020-07-28', '2020-09-12', '2020-09-12', '0', 2),
(36, '412204', '23', 2, 3, '2020-07-29', '2020-09-30', '2020-09-30', '0', 2),
(37, '108863', '23', 2, 1, '2020-08-01', '2020-08-03', '2020-08-03', '0', 2),
(38, '108863', '23', 3, 3, '2020-08-01', '2020-08-03', '2020-08-02', '0', 2),
(39, '108863', '23', 4, 1, '2020-08-01', '2020-08-03', '2020-08-02', '20000', 2),
(40, '407282', '23', 7, 3, '2020-08-01', '2020-08-05', '2020-08-10', '100000', 2),
(41, '856203', '23', 7, 1, '2020-08-08', '2020-09-29', '0000-00-00', '0', 1),
(42, '856203', '23', 6, 1, '2020-08-08', '2020-09-29', '0000-00-00', '0', 1),
(43, '856203', '23', 5, 1, '2020-08-08', '2020-09-29', '0000-00-00', '0', 1),
(45, '993159', '23', 3, 2, '2020-07-07', '2020-07-27', '2020-07-28', '20000', 2),
(46, '425744', '23', 3, 3, '2020-07-27', '2020-07-27', '2020-07-27', '0', 2),
(47, '173986', '23', 2, 2, '2020-07-27', '2020-01-12', '2020-07-27', '3940000', 2),
(48, '234142', '23', 6, 2, '2020-07-27', '2020-07-28', '2020-07-27', '20000', 2),
(49, '373033', '23', 7, 1, '2020-07-27', '2020-07-29', '2020-07-28', '20000', 2),
(50, '951516', '23', 2, 1, '2020-07-27', '2020-08-29', '2020-08-29', '0', 2),
(51, '272618', '23', 2, 1, '2020-07-28', '2020-08-29', '2020-08-27', '40000', 2),
(53, '446532', '23', 3, 1, '2020-07-28', '2020-07-30', '2020-07-29', '20000', 2),
(54, '382026', '23', 3, 2, '2020-07-28', '2020-09-29', '2020-07-28', '1260000', 2),
(55, '614983', '23', 2, 40, '2020-07-28', '2020-09-12', '2020-09-12', '0', 2),
(56, '412204', '23', 2, 3, '2020-07-29', '2020-09-30', '2020-09-30', '0', 2),
(57, '108863', '23', 2, 1, '2020-08-01', '2020-08-03', '2020-08-03', '0', 2),
(58, '108863', '23', 3, 3, '2020-08-01', '2020-08-03', '2020-08-02', '0', 2),
(59, '108863', '23', 4, 1, '2020-08-01', '2020-08-03', '2020-08-02', '20000', 2),
(60, '407282', '23', 7, 3, '2020-08-01', '2020-08-05', '2020-08-10', '100000', 2),
(61, '856203', '23', 7, 1, '2020-08-08', '2020-09-29', '0000-00-00', '0', 1),
(62, '856203', '23', 6, 1, '2020-08-08', '2020-09-29', '0000-00-00', '0', 1),
(63, '856203', '23', 5, 1, '2020-08-08', '2020-09-29', '0000-00-00', '0', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pertanyaan_keamanan`
--

CREATE TABLE `tb_pertanyaan_keamanan` (
  `pertanyaan_id` int(11) NOT NULL,
  `pertanyaan_user` int(11) NOT NULL,
  `pertanyaan` varchar(255) NOT NULL,
  `pertanyaan_jawaban` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pertanyaan_keamanan`
--

INSERT INTO `tb_pertanyaan_keamanan` (`pertanyaan_id`, `pertanyaan_user`, `pertanyaan`, `pertanyaan_jawaban`) VALUES
(4, 22, 'Nama sekolah SD anda adalah?', 'Gandasari'),
(5, 23, 'Siapa nama peliharaan anda?', 'user'),
(6, 24, 'Siapa nama peliharaan anda?', 'admin'),
(7, 25, 'Nama sekolah SD anda adalah?', 'cinyosog2'),
(8, 26, 'Siapa nama saudara anda?', 'faqih'),
(9, 27, 'Nama sekolah SD anda adalah?', 'cinyosog2'),
(10, 28, 'Nama sekolah SD anda adalah?', 'cinyosog2'),
(11, 29, 'Nama sekolah SD anda adalah?', 'cinyosog2'),
(12, 30, 'Siapa nama kakek anda?', 'Slamet');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sub`
--

CREATE TABLE `tb_sub` (
  `sub_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_judul` varchar(255) NOT NULL,
  `sub_link` varchar(255) NOT NULL,
  `sub_icon` varchar(255) NOT NULL,
  `sub_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_sub`
--

INSERT INTO `tb_sub` (`sub_id`, `menu_id`, `sub_judul`, `sub_link`, `sub_icon`, `sub_status`) VALUES
(3, 2, 'Menu', 'menu', 'fas fa-fw fa-bars', 1),
(4, 2, 'Sub Menu', 'subMenu', 'fas fa-fw fa-ellipsis-v', 1),
(6, 12, 'Data Petugas', 'dataPetugas', 'fas fa-fw fa-user', 1),
(7, 13, 'Data User', 'dataUser', 'fas fa-fw fa-users', 1),
(8, 14, 'Data Buku', 'dataBuku', 'fas fa-fw fa-book', 1),
(9, 14, 'Katalog Buku', 'katalogBuku', 'fas fa-fw fa-bookmark', 1),
(10, 15, 'Data Peminjaman Buku', 'peminjamanBuku', 'fas fa-fw fa-book-reader', 1),
(11, 15, 'Data Booking', 'dataBooking', 'fas fa-fw fa-comments-dollar', 1),
(12, 16, 'Website Settings', 'websiteSettings', 'fas fa-fw fa-info-circle', 1),
(13, 13, 'Log User', 'logUser', 'fas fa-fw fa-sign-in-alt', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `user_id` int(11) NOT NULL,
  `user_noId` varchar(20) NOT NULL,
  `user_nama` varchar(255) NOT NULL,
  `user_tempatLahir` varchar(20) NOT NULL,
  `user_tanggalLahir` date NOT NULL,
  `user_klasifikasi` varchar(20) NOT NULL,
  `user_ktp` varchar(255) NOT NULL,
  `user_foto` varchar(255) NOT NULL,
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_role` int(11) NOT NULL,
  `user_noHP` varchar(20) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_qr` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `user_noId`, `user_nama`, `user_tempatLahir`, `user_tanggalLahir`, `user_klasifikasi`, `user_ktp`, `user_foto`, `user_username`, `user_password`, `user_role`, `user_noHP`, `user_email`, `user_qr`) VALUES
(22, '875693', 'Faqih Pratama Muhti', '', '1995-01-12', '', '33290441570004', 'Faqih_pM.jpg', 'suadmin', '$2y$10$p4o8RX5Qf5KpiMdalNMzQOiPs6bH98C1g3juc5SBYwlRdssDVNF1y', 1, '081212499837', 'faqihpratamamuhti@gmail.com', '875693.png'),
(23, '0002', 'Faqih', 'Jakarta', '1995-01-12', 'Mahasiswa', '3325641241242', 'Faqih_Pratama_Muhti.png', 'user', '$2y$10$XVvkxa422L8dP/tFltUf3e7MDAdC0CcEqZmso3XLZLtoBe2K/Y1RW', 3, '081212499837', 'si18.faqihmuhti@mhs.ubpkarawang.ac.id', '0002.png'),
(24, 'P-0001', 'Muhti', 'Bekasi', '1995-01-12', 'PNS', '332654521541', 'Faqih_pM.jpg', 'admin', '$2y$10$EvegfYYp7/FlLCzyPeY1DO3xG0CxpY5Wu4ddivr6iTznD3cgMfGai', 2, '081245124151', 'admin@mail.com', 'P-0001.png'),
(29, '0005', 'Fitrya', 'Cileungsi', '2013-09-29', 'SD', '0123456789', 'Fitrya.png', 'fitrya', '$2y$10$dQmnxEpOzO1pWImdgrdigOWGh0nXfs4CwFKGeQp.jC89r/18lcaFa', 3, '081212499837', 'fitrya@gmail.com', '0005.png'),
(30, '0009', 'Fadli', 'Jakarta', '1995-01-12', 'Karyawan', '987654321', 'profil_(2).jpg', 'f1__m', '$2y$10$8Hf2bQXWTr7Imq4M1hpFleqAJv/Q2ev.ZIJNi5P8Jn/OEmL6et50W', 3, '081212499837', 'fadlie@gmail.com', '0009.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_website`
--

CREATE TABLE `tb_website` (
  `website_id` int(11) NOT NULL,
  `website_jum` text NOT NULL,
  `website_subjum` text NOT NULL,
  `website_gbrjum` varchar(255) NOT NULL,
  `website_tentang` text NOT NULL,
  `website_kontak` tinytext NOT NULL,
  `website_email` varchar(50) NOT NULL,
  `website_alamat` varchar(50) NOT NULL,
  `website_wa` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_website`
--

INSERT INTO `tb_website` (`website_id`, `website_jum`, `website_subjum`, `website_gbrjum`, `website_tentang`, `website_kontak`, `website_email`, `website_alamat`, `website_wa`) VALUES
(1, 'Selamat datang di Perpustakaan Pemda Karawang', 'Gudangnya ilmu pengetahuan', 'Karawang.png', '<p><strong>Perpustakan Daerah Kabupaten Karawang</strong>&nbsp;bisa menjadi penggerak terciptanya budaya baca, tulis dan budaya menghargai bahan bacaan oleh masyarakat di daerah tersebut.</p>\r\n\r\n<p><strong>Perpustakan Daerah Kabupaten Karawang</strong> akan terus meningkatkan mutu pelayanannya agar dapat menarik minat pengunjung untuk datang. Selain itu, akan terus berinovasi demi terciptanya minat baca bagi masyarakat Kabupaten Karawang.</p>\r\n', 'Assalamu`alaikum Warahmatullahi Wabarakatuh', 'info@perpusnas.ac.id', 'Jl. Jendral A. Yani No. 10 Karawang Barat', '081212499837');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_access`
--
ALTER TABLE `tb_access`
  ADD PRIMARY KEY (`access_id`);

--
-- Indeks untuk tabel `tb_booking`
--
ALTER TABLE `tb_booking`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indeks untuk tabel `tb_buku`
--
ALTER TABLE `tb_buku`
  ADD PRIMARY KEY (`buku_id`);

--
-- Indeks untuk tabel `tb_cart`
--
ALTER TABLE `tb_cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indeks untuk tabel `tb_denda`
--
ALTER TABLE `tb_denda`
  ADD PRIMARY KEY (`denda_id`);

--
-- Indeks untuk tabel `tb_identitas_orangtua`
--
ALTER TABLE `tb_identitas_orangtua`
  ADD PRIMARY KEY (`orangtua_id`);

--
-- Indeks untuk tabel `tb_log`
--
ALTER TABLE `tb_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indeks untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indeks untuk tabel `tb_peminjaman`
--
ALTER TABLE `tb_peminjaman`
  ADD PRIMARY KEY (`peminjaman_id`);

--
-- Indeks untuk tabel `tb_pertanyaan_keamanan`
--
ALTER TABLE `tb_pertanyaan_keamanan`
  ADD PRIMARY KEY (`pertanyaan_id`);

--
-- Indeks untuk tabel `tb_sub`
--
ALTER TABLE `tb_sub`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeks untuk tabel `tb_website`
--
ALTER TABLE `tb_website`
  ADD PRIMARY KEY (`website_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_access`
--
ALTER TABLE `tb_access`
  MODIFY `access_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `tb_booking`
--
ALTER TABLE `tb_booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT untuk tabel `tb_buku`
--
ALTER TABLE `tb_buku`
  MODIFY `buku_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tb_cart`
--
ALTER TABLE `tb_cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_denda`
--
ALTER TABLE `tb_denda`
  MODIFY `denda_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_identitas_orangtua`
--
ALTER TABLE `tb_identitas_orangtua`
  MODIFY `orangtua_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tb_log`
--
ALTER TABLE `tb_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tb_peminjaman`
--
ALTER TABLE `tb_peminjaman`
  MODIFY `peminjaman_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT untuk tabel `tb_pertanyaan_keamanan`
--
ALTER TABLE `tb_pertanyaan_keamanan`
  MODIFY `pertanyaan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tb_sub`
--
ALTER TABLE `tb_sub`
  MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `tb_website`
--
ALTER TABLE `tb_website`
  MODIFY `website_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
