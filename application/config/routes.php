<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route["logout"] = "index/logout";

$route["pinjam_buku/(.+)"] = "index/pinjam_buku/$1";
$route["koleksi_buku"] = "index/koleksi_buku";
$route["koleksi_buku/(.+)"] = "index/koleksi_buku/$1";

$route["pinjaman_saya"] = "index/pinjaman_saya";
$route["process_cart"] = "index/process_cart";
$route["buku_saya"] = "index/buku_saya";

$route["cari_buku"] = "index/search_buku";
$route["saran"] = "index/saran";

$route["myprofile"] = "index/myprofile";
$route["profilePassword"] = "index/profilePassword";

$route["profile"] = "profile/editProfile";
$route["editProfile"] = "profile/editProfile";
$route["gantiPassword"] = "profile/gantiPassword";

$route['login'] = 'welcome/login';
$route['register'] = 'welcome/register';
$route['lupaPassword'] = 'welcome/lupaPassword';
$route['pertanyaan'] = 'welcome/pertanyaan';
$route['resetPassword'] = 'welcome/resetPassword';

$route['admin'] = 'admin';
$route['menu'] = 'admin/view_menu';
$route["tambah_menu"] = "admin/tambah_menu";
$route['view_menu_edit/(.+)'] = 'admin/view_menu_edit/$1';
$route['process_menu_edit'] = 'admin/process_menu_edit';
$route['process_menu_delete/(.+)'] = 'admin/process_menu_delete/$1';

$route['access_add'] = 'admin/process_access_add';
$route['access_delete/(.+)'] = 'admin/process_access_delete/$1';
$route['access_edit/(.+)'] = 'admin/view_access_edit/$1';
$route['validation_access_edit'] = 'admin/validation_access_edit';

$route['subMenu'] = 'admin/view_sub_menu';
$route['validation_sub_add'] = 'admin/validation_sub_add';
$route['edit_sub/(.+)'] = 'admin/view_sub_edit/$1';
$route['validation_sub_edit'] = 'admin/validation_sub_edit';
$route['delete_sub/(.+)'] = 'admin/process_sub_delete/$1';

$route['dataPetugas'] = "admin/view_petugas";
$route["petugas_edit/(.+)"] = "admin/view_petugas_edit/$1";
$route["validation_petugas_edit"] = "admin/validation_petugas_edit";
$route["validation_petugas_add"] = "admin/validation_petugas_add";
$route["petugas_delete/(.+)"] = "admin/process_petugas_delete/$1";

$route["dataAnggota"] = "user";
$route["validation_user_add"] = "user/validation_user_add";
$route["user_edit/(.+)"] = "user/view_user_edit/$1";
$route["validation_user_edit"] = "user/validation_user_edit";
$route["user_delete/(.+)"] = "user/process_user_delete/$1";
$route["cetakUser/(.+)"] = "user/cetakUser/$1";
$route["logUser"] = "user/view_log_user";

$route["katalogBuku"] = "buku/view_katalog";
$route["validation_katalog_add"] = "buku/validation_katalog_add";
$route["process_katalog_hapus/(.+)"] = "buku/process_katalog_delete/$1";
$route["katalog_edit/(.+)"] = "buku/katalog_edit/$1";
$route["validation_katalog_edit"] = "buku/validation_katalog_edit";

$route["dataBuku"] = "buku/view_buku";
$route["export_buku"] = "buku/export_buku";
$route["export_katalog"] = "buku/export_katalog";
$route["import_katalog"] = "buku/import_katalog";

$route["peminjamanBuku"] = "manajemenbuku/view_peminjaman";
$route["validation_peminjaman_add"] = "manajemenbuku/validation_peminjaman_add";
$route["peminjaman_dikembalikan/(.+)"] = "manajemenbuku/peminjaman_dikembalikan/$1";
$route["validation_peminjaman_kembali"] = "manajemenbuku/validation_peminjaman_kembali";
$route["peminjaman_batal/(.+)"] = "manajemenbuku/peminjaman_batal/$1";
$route["peminjaman_hapus/(.+)"] = "manajemenbuku/peminjaman_hapus/$1";
$route["search_buku"] = "manajemenbuku/search_buku";
$route["search_user"] = "manajemenbuku/search_user";

$route["export_peminjaman"] = "manajemenbuku/export_peminjaman";
$route["import_peminjaman"] = "manajemenbuku/import_peminjaman";

$route["denda"] = "manajemenbuku/denda";
$route["validation_denda_edit"] = "manajemenbuku/validation_denda_edit";

$route["dataBooking"] = "manajemenbuku/view_booking";
$route["process_booking_tolak/(.+)"] = "manajemenbuku/process_booking_tolak/$1";
$route["process_booking_terima/(.+)"] = "manajemenbuku/process_booking_terima/$1";
$route["process_booking_delete/(.+)"] = "manajemenbuku/process_booking_delete/$1";

$route["websiteSettings"] = "website";
$route["validation_website_edit"] = "website/validation_website_edit";