<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid konten">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card">
        <div class="card-body">
          <h5>Ganti Password</h5>
          <?= form_open('profilePassword'); ?>
          <div class="form-group">
            <?= form_label('Password lama'); ?>
            <?= form_password('passLama','','class="form-control"'); ?>
            <?= form_error('passLama','<small class="text-danger">','</small>'); ?>
          </div>
          <div class="form-group">
            <?= form_label('Password baru'); ?>
            <?= form_password('passBaru','','class="form-control"'); ?>
            <?= form_error('passBaru','<small class="text-danger">','</small>'); ?>
          </div>
          <div class="form-group">
            <?= form_label('Masukan kembali password baru'); ?>
            <?= form_password('retypeBaru','','class="form-control"'); ?>
            <?= form_error('retypeBaru','<small class="text-danger">','</small>'); ?>
          </div>
          <?= form_submit('Ganti','Ganti','class="btn btn-success btn-sm"') ?>
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>