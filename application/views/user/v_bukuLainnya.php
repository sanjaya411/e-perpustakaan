<div class="container-fluid konten">
  <div class="container-fluid card shadow bg-dark justify-content-center text-light" data-aos="fade-down" data-aos-duration="1500"><h4 class="text-center" >Koleksi Buku</h4></div>
    <div class="container-fluid card shadow bg-dark justify-content-center" data-aos="fade-up" data-aos-duration="1500"><p></p>
      <div class="container-fluid card shadow justify-content-center">
        <div class="row d-flex flex-wrap justify-content-center">
          <?php foreach($buku as $item) { ?>
            <div class="col-md-2">
              <div  data-aos="fade-right" data-aos-duration="1500" class="card user shadow mr-auto ml-md-2 my-1 my-md-2 mw-100" >
                <img src="<?= base_url('vendor/img/buku/'.$item->buku_foto); ?>" alt="" class="card-img-top gbr-buku">
                <div class="card-body mt-100 md-100 mx-auto my-auto mw-auto" >
                  <div class="card-title mt-100 md-100 mx-auto my-auto mw-auto">
                  <h6 style="font-size: 14px;"><?= $item->buku_judul; ?></h6>
                  <h6 style="font-size: 14px;">Penerbit: <?= $item->buku_penerbit; ?></h6>
                  <h6 style="font-size: 14px;">Tahun Terbit: <?= $item->buku_tahunTerbit; ?></h6>
                  <h6 style="font-size: 14px;">Stok: <?= $item->buku_stok; ?></h6>
                  </div> 
                </div>
                <?php
                  $stok=$item->buku_stok;
                    if ($stok <= 0) {?>
                    <?php
                    } else {?>
                      <a href="<?= base_url('pinjam_buku/'.$item->buku_id); ?>" class="btn btn-sm btn-success">Pinjam</a>
                    <?php 
                    }
                ?>
              </div>
            </div>
          <?php } ?>     
        </div>  
        <?= $link; ?>
      </div><p></p>
    </div>
  </div>
<br><br><br>
</div>