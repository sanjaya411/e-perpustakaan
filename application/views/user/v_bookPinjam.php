<?= $this->session->flashdata('pesan'); ?>
<br>

<div class="container-fluid konten">
  <h4 class="mt-5">Data Booking Anda</h4>
  <div data-aos="fade-right" data-aos-duration="1000" class="card shadow badge-danger">
      <p></p><p><b>&nbsp;&nbsp;&nbsp;NOTE </b>--> Jika anda meminjam buku, segera datang ke perpustakaan sebelum jam Maksimal ke perpustakaan agar buku anda tidak diambil oleh pengguna lainnya.</p>
    </div><br>
  <div class="card shadow "  data-aos="fade-left" data-aos-duration="1000">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered" id="data">
        <thead>
          <tr>
            <th width=1%>Nomor</th>
            <th>Nomor&nbsp;Booking</th>
            <th>Buku</th>
            <th>Penerbit</th>          
            <th width=1%>Tahun&nbsp;Terbit</th>
            <th width=1%>Jumlah</th>
            <th>Waktu</th>
            <th width=1%>Maksimal&nbsp;ke&nbsp;perpustakaan</th>
            <th width=1%>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($list_booking as $item) : ?>
          <tr>
            <td><?= $no++; ?></td>
            <td>BK-<?= $item->booking_noId; ?></td>
            <td><?= $item->buku_judul; ?></td>
            <td><?= $item->buku_penerbit; ?></td>
            <td><?= $item->buku_tahunTerbit; ?></td>
            <td><?= $item->booking_jumlah; ?></td>
            <td><?= date('d M Y H:i:s', strtotime($item->booking_waktu)); ?></td>
            <td><?= date("d M Y H:i:s", strtotime($item->booking_expired)); ?></td>
            <td>
              <?php
                if($item->booking_accept == 0) {
                  echo '<div class="badge shadow badge-info">Tunggu dikonfirmasi</div>';
                } elseif($item->booking_accept == 1) {
                  echo '<div class="badge shadow badge-success">Dikonfirmasi</div>';
                } else {
                  echo '<div class="badge shadow badge-danger">Ditolak</div>';
                }
              ?>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      </div>
    </div>
  </div>
</div>

<!-- Datatables -->
<script src="<?= base_url('vendor/datatables/js/jquery.dataTables.js'); ?>"></script>
<script src="<?= base_url('vendor/datatables/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script>
  $("#data").dataTable();
</script>