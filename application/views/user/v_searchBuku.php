<div class="container-fluid konten">
  <h4>Hasil Pencarian Buku</h4>
  <div class="row mt-5">
    <?php foreach($list_buku as $item) { ?>
    <div class="col-md-3 mb-4">
      <div class="card shadow" style="height: 300px">
        <img src="<?php echo base_url("vendor/img/buku/".$item->buku_foto); ?>" alt="" class="card-img-top d-block mx-auto" style="width: 50%; max-height: 50%">
        <div class="card-body">
          <h5 class="card-header-title"><?php echo $item->buku_judul ?></h5>
          <p>Penerbit: <?php echo $item->buku_penerbit; ?> <br>
            Tahun Terbit: <?php echo $item->buku_tahunTerbit;  ?> <br>
            Stok : <?php echo $item->buku_stok; ?>
          </p>
        </div>
        <a href="<?php echo base_url("pinjam_buku/".$item->buku_id); ?>" class="card-footer text-white py-2 text-decoration-none bg-success text-center">
          Pinjam
        </a>
      </div>
    </div>
    <?php } ?>
  </div>
</div>