<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid konten">
  <div class="row mt-4">
    <div class="col-md-6 mb-4">
      <div class="card">
        <div class="card-body">
          <img src="<?= base_url('vendor/img/user/'.$u->user_foto); ?>" alt="Foto Profile" class="d-block mx-auto rounded-circle gbr-profile">
          <hr>
          <table class="table">
            <tr>
              <th>Kode Anggota</th>
              <td>: <?= $u->user_noId; ?></td>
            </tr>
            <tr>
              <th>Nama</th>
              <td>: <?= $u->user_nama; ?></td>
            </tr>
            <tr>
              <th>Username</th>
              <td>: <?= $u->user_username; ?></td>
            </tr>
            <tr>
              <th>No Hp</th>
              <td>: <?= $u->user_noHP; ?></td>
            </tr>
            <tr>
              <th>Email</th>
              <td>: <?= $u->user_email; ?></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <h5>Edit Profile</h5>
          <?= form_open_multipart('myprofile'); ?>
          <div class="form-group">
            <label>Foto</label>
            <?= form_upload('foto','','class="form-control form-control-file"'); ?>
          </div>
          <div class="form-group">
            <label>Nama</label>
            <?= form_input('nama',$u->user_nama,'class="form-control"'); ?>
            <?= form_error('nama','<small class="text-danger">','</small>') ?>
          </div>
          <div class="form-group">
            <label>Username</label>
            <?= form_input('username',$u->user_username,'class="form-control" disabled') ?>
          </div>
          <div class="form-group">
            <label>Nomor HP</label>
            <?= form_input('hp',$u->user_noHP,'class="form-control"'); ?>
            <?= form_error('hp','<small class="text-danger">','</small>') ?>
          </div>
          <div class="form-group">
            <label>Email</label>
            <?= form_input('email',$u->user_email,'class="form-control"') ?>
            <?= form_error('email','<small class="text-danger">','</small>') ?>
          </div>
          <?= form_submit('submit','Ubah','class="btn btn-success btn-sm"') ?>
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>