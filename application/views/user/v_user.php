<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <p></p>
  <div class="container-fluid konten card justify-content-center shadow"><p></p>
    <div class="container-fluid card shadow bg-dark"><p></p>
      <div class="row"><br><br>
        <div data-aos="fade-right" data-aos-duration="1000" class="col-md-6 d-flex flex-column  align-items-center justify-content-center"><br><br>
          <h4 class="text-light"><?= $website->website_jum; ?></h4>
          <p class="text-light"><?= $website->website_subjum ?><br><br><br>
        </div>
        <div class="col-md-6"><br><br>
          <img data-aos="fade-left" data-aos-duration="1000" style="width:120px; height:150px" src="<?= base_url('vendor/img/website/'.$website->website_gbrjum); ?>" alt="" class="gbr"><br>
        </div>
      </div><br><br>
    </div><br>
  </div>
  <br>
  <div class="container-fluid shadow justify-content-center" >
    <div class="row shadow justify-content-center">
      <div class="col-md-6 card shadow" data-aos="fade-right" data-aos-duration="1000"><p></p>
        <h4 class="text-center" >Halo, perkenalkan kami</h4>
        <div  class="text-justify">
        <?= $website->website_tentang?>
        </div>
      </div>
      <div class="col-md-6 card shadow " data-aos="fade-left" data-aos-duration="1000"><p></p>
        <h4 class="text-center ">Kontak kami</h4>
        <div  class="text-justify">
          <?= $website->website_kontak; ?>
          <p><i class="fas fa-map-marker-alt fa-lg mr-2"></i><?= $website->website_alamat; ?></p>
          <p><i class="fas fa-phone-square-alt fa-lg mr-2"></i><?= $website->website_wa; ?></p>
          <p><i class="fas fa-envelope-open-text fa-lg mr-2"></i><?= $website->website_email; ?></p>
        </div>
      </div>
    </div>
  </div>
  <br><br id="galeri"><br><br><br><br>
<div class="container-fluid card shadow bg-dark justify-content-center text-light" data-aos="fade-down" data-aos-duration="1500" ><h4 class="text-center">Galeri</h4></div>
  <div  class="container-fluid card shadow bg-dark justify-content-center" data-aos="fade-up" data-aos-duration="1500"><p></p>
    <div   class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div  class="carousel-item active" >
            <img  class="d-block shadow w-100" src="<?= base_url('vendor/img/website/view-a.jpg'); ?>" alt="First slide">
            <div class="carousel-caption">
              <h2 class="xs-shadow" data-aos="fade-right" data-aos-duration="1000">Perpusda&nbsp;Karawang</h2>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block xs-shadow w-100" src="<?= base_url('vendor/img/website/view-b.jpg'); ?>" alt="Second slide">
            <div class="carousel-caption">
              <h2 class="xs-shadow" data-aos="fade-right" data-aos-duration="1000">Ruang Layanan&nbsp;Umum</h2>
            </div>
        </div>
        <div class="carousel-item ">
            <img  class="d-block xs-shadow w-100" src="<?= base_url('vendor/img/website/view-c.jpg'); ?>" alt="Third slide">
            <div class="carousel-caption">
              <h2 class="xs-shadow" data-aos="fade-right" data-aos-duration="1000">Ruang&nbsp;Anak-Anak</h2>
            </div>
        </div>
        <div class="carousel-item ">
            <img  class="d-block xs-shadow w-100" src="<?= base_url('vendor/img/website/view-e.jpg'); ?>" alt="Fourth slide">
            <div class="carousel-caption">
              <h2 class="xs-shadow" data-aos="fade-right" data-aos-duration="1000">Ruang&nbsp;Rak&nbsp;Buku</h2>
            </div>
        </div>
        <div class="carousel-item ">
            <img  class="d-block xs-shadow w-100" src="<?= base_url('vendor/img/website/view-d.jpg'); ?>" alt="Fourth slide">
            <div class="carousel-caption">
              <h2 class="xs-shadow" data-aos="fade-right" data-aos-duration="1000">Ruang&nbsp;Referensi</h2>
            </div>
        </div>
      </div><p id="buku"></p>
    </div>
  </div>
  <br><br><br><br><br><br>  
  <div class="container-fluid card shadow bg-dark justify-content-center text-light" data-aos="fade-down" data-aos-duration="1500"><h4 class="text-center">Koleksi Buku</h4></div>
  <div class="container-fluid card shadow bg-dark justify-content-center" data-aos="fade-up" data-aos-duration="1500" id="buku"><p></p>
    <div  class="container-fluid card shadow justify-content-center" >
      <div class="row mt-2 md-6 justify-content-center" >
        <?php foreach($list_buku as $item) : ?>
        <div class="col-md-2">
          <div class="card user shadow mr-auto ml-md-2 my-1 my-md-2 mw-100" data-aos="fade-left" data-aos-duration="3000">
            <img  src="<?= base_url('vendor/img/buku/'.$item->buku_foto); ?>" alt="" class="card-img-top gbr-buku">
            <div class="card-body mt-100 md-100 mx-auto">
              <div class="card-title mt-100 md-100 ">
              <h6 style="font-size: 14px;"><?= $item->buku_judul; ?></h6>
              <h6 style="font-size: 14px;">Penerbit: <?= $item->buku_penerbit; ?></h6>
              <h6 style="font-size: 14px;">Tahun Terbit: <?= $item->buku_tahunTerbit; ?></h6>
              <h6 style="font-size: 14px;">Stok: <?= $item->buku_stok; ?></h6>
              </div> 
            </div>
            <?php
                $stok=$item->buku_stok;
                  if ($stok <= 0) {?>
                  <?php
                  } else {?>
                    <a href="<?= base_url('pinjam_buku/'.$item->buku_id); ?>" class="btn btn-success btn-sm">Pinjam</a>
                  <?php 
                  }
                  ?>
          </div>
        </div>
        <?php endforeach; ?>
        <div class="row mt-3 justify-content-center" data-aos="fade-right" data-aos-duration="1500">
          <a href="koleksi_buku" class="btn btn-primary shadow btn-sm-5">Lihat lainnya ></a>
        </div>
      </div><p></p>
    </div><p></p>
  </div><p></p>
  <br><br><br>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-8 mx-auto">
    <h4 class="text-center mb-3" data-aos="fade-right" data-aos-duration="1500">Masukan & Saran</h4>
      <?php echo form_open("saran"); ?>
        <div class="form-group" data-aos="fade-right" data-aos-duration="1500">
          <div class="row mb-3">
            <div class="col-md-6">
              <input type="text" name="nama_depan" class="form-control" placeholder="Nama depan">
            </div>
            <div class="col-md-6">
              <input type="text" name="nama_belakang" class="form-control" placeholder="Nama belakang">
            </div>
          </div>
          <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Alamat email">
          </div>
          <div class="form-group">
            <input type="text" name="subjek" class="form-control" placeholder="Subjek">
          </div>
          <div class="form-group">
            <textarea name="pesan" class="form-control" cols="30" rows="5" placeholder="Masukan pesan"></textarea>
          </div>
        </div>
        <?php echo form_submit("submit", "Submit", "class='btn btn-success btn-sm'") ?>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>