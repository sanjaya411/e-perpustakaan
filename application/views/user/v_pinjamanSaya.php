<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid konten">
  <h4 class="mt-5">Keranjang peminjaman anda</h4>
  <div class="table-responsive card shadow" data-aos="fade-left" data-aos-duration="1000">
    <table class="table table-striped table-hover table-bordered order-table ml-md-2 my-1 my-md-2" id="data">
      <thead>
        <tr>
          <th width=1%>Nomor</th>
          <th>Buku</th>
          <th>Penerbit</th>          
          <th width=1%>Tahun&nbsp;Terbit</th>
          <th width=1%>Jumlah</th>
        </tr>
      </thead>
      <tbody>
        <div class='d-none d-sm-inline-block form-inline mr-auto ml-md-2 my-1 my-md-2 mw-100 navbar-search'>
          <?php echo form_open('pinjaman_saya')?>
            <div class="input-group-append">
            <input type='serach' placeholder='Pencarian ...' name='kata' aria-label="Search" class='search-input form-control my-6 mr-sm-1' required/>
              <button class="btn btn-primary" name='cari' type="submit">
                <i class="fas fa-search fa-sm"></i>
              </div>
          </form>
        </div>
      <!-- <div class='form-inline my-2 my-lg-1'>
        <?php echo form_open('pinjaman_saya')?>
          &nbsp;<input type='serach' placeholder='Pencarian ...' name='kata' aria-label="Search" class='search-input form-control my-6 mr-sm-1' required/>
          <input type='submit' value='Search' name='cari' class='btn btn-outline-info'/>
        </form>
      </div> -->
        <?php $no=1; foreach($list_cart as $item) : ?>
        <tr>
          <td><?= $no++; ?></td>
          <td><?= $item->buku_judul; ?></td>
          <td><?= $item->buku_penerbit; ?></td>
          <td><?= $item->buku_tahunTerbit; ?></td>
          <td><?= $item->cart_jumlah; ?></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div><br>
  <div class="row">
    <div class="col-md-4" data-aos="fade-right" data-aos-duration="1000">
      <?= form_open('process_cart'); ?>
      <div class="form-group" >
        <label>Tanggal Dikembalikan</label>
        <input type="date" name="dikembalikan" class="form-control">
      </div>
      <input type="submit" value="Booking Buku" class="btn btn-success  shadow btn-sm">
      <?= form_close(); ?>
    </div>
  </div>
</div>