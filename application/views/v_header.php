<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?= base_url('vendor/css/bootstrap.min.css'); ?>">

  <!-- AOS -->
  <link rel="stylesheet" href="<?= base_url('vendor/css/aos.css'); ?>">

  <!-- Custom -->
  <link rel="stylesheet" href="<?= base_url('vendor/css/custom.css'); ?>">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('vendor/font-awesome/css/all.min.css'); ?>">

  <!-- icon Title -->
  <link rel="icon" href="<?= base_url('vendor/img/Icon/Karawang.png'); ?>">

  <!-- jQuery -->
  <script src="<?= base_url('vendor/vendor/jquery/jquery.min.js') ?>"></script>

  <script src="<?= base_url('vendor/js/sweet.js'); ?>"></script>
  <script src="<?= base_url('/vendor/js/custom.js'); ?>"></script>

  <title>Perpusda Karawang</title>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <div class="container">
    <a href="<?= base_url('index'); ?>" class="navbar-brand">Perpustakaan</a>
    <button class="navbar-toggler" data-toggle="collapse" data-target="#navbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a href="<?= base_url('index') ?>" class="nav-link active">Home</a>
        </li>
        <li class="nav-item">
          <a href="<?= base_url('index#galeri') ?>" class="nav-link">Galeri</a>
        </li>
        <li class="nav-item">
          <a href="<?= base_url('koleksi_buku') ?>" class="nav-link">Koleksi Buku</a>
        </li>
        <?php
          if($this->session->userdata('admin_id')) {
        ?>
        <li class="nav-item">
          <a href="<?= base_url('pinjaman_saya') ?>" class="nav-link">Peminjaman (<?= $total_pinjaman; ?>)</a>
        </li>
        <li class="nav-item">
          <a href="<?= base_url('buku_saya') ?>" class="nav-link">Buku Saya (<?= $total_buku_saya; ?>)</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">
            Profile
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="<?= base_url('myprofile') ?>">Profile</a>
            <a class="dropdown-item" href="<?= base_url('profilePassword') ?>">Ganti Password</a>
          </div>
        </li>
        <?php } ?>
      </ul>

      <nav class="navbar-nav ml-auto">
        <div class='form-inline'>
          <form action="<?php echo base_url("cari_buku"); ?>" method="GET">
            <div class="input-group-append">
            <input type='serach' placeholder='Cari Buku ...' name='buku' aria-label="Search" class='search-input form-control my-6 mr-sm-1' required/>
              <button class="btn btn-primary" type="submit">
                <i class="fas fa-search fa-sm"></i>
              </div>
          </form>
        </div> &nbsp;&nbsp;&nbsp;
        <!-- <form class="form-inline my-2 my-lg-0">
          <input class="form-control form-control-sm mr-sm-2 search-input" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-info my-2 my-sm-0 mr-2 btn-sm" type="submit">Search</button>
        </form> -->
        <?php if($this->session->userdata('admin_id')) : ?>
        <a href="<?= base_url('logout'); ?>" class="btn btn-outline-danger my-2">Logout</a>
        <?php else : ?>
          <a href="<?= base_url('login'); ?>" class="btn btn-outline-primary my-2">Login</a>&nbsp;
        <a href="<?= base_url('register'); ?>" class="btn btn-outline-success my-2">Register</a>
        <?php endif; ?>
      </nav>
    </div>
  </div>
</nav>