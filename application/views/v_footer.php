  <!-- Core Bootstrap -->
  <script src="<?= base_url('vendor/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
  <script src="<?= base_url('vendor/vendor/bootstrap/js/cari.js'); ?>"></script>

  <!-- Font Awesome -->
  <script src="<?= base_url('vendor/font-awesome/js/all.min.js') ?>"></script>

  <!-- AOS -->
  <script src="<?= base_url('vendor/js/aos.js'); ?>"></script>
  <script>
    AOS.init();
  </script>
</body>
</html>