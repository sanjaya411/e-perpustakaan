<div class="container-fluid">
  <div class="row">
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Buku</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $total_buku; ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-book fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Peminjaman Buku</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $total_peminjaman; ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-book-reader fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Data Booking</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $total_booking; ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-comments-dollar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Anggota</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $total_anggota; ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-user fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <h5>Data klasifikasi user</h5>
          <div class="chart-pie pt-4 pb-2">
            <canvas id="chartKlasifikasi"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mb-3 justify-content-center">
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <h5>Perbandingan peminjaman & booking</h5>
          <div class="chart-pie pt-4 pb-2">
            <canvas id="chartBuku"></canvas>
          </div>
          <div class="mt-4 text-center small">
            <span class="mr-2">
              <i class="fas fa-circle text-success"></i> Peminjaman
            </span>
            <span class="mr-2">
              <i class="fas fa-circle text-primary"></i> Booking
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <h5>Perbandingan jumlah pengunjung & User hari ini</h5>
          <div class="chart-pie pt-4 pb-2">
            <canvas id="chartUser"></canvas>
          </div>
          <div class="mt-4 text-center small">
            <span class="mr-2">
              <i class="fas fa-circle text-success"></i> Pengunjung
            </span>
            <span class="mr-2">
              <i class="fas fa-circle text-primary"></i> User
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <h5>Data statistik utama</h5>
          <div class="chart-pie pt-4 pb-2">
            <canvas id="myPieChart"></canvas>
          </div>
          <div class="mt-4 text-center small">
            <span class="mr-2">
              <i class="fas fa-circle text-primary"></i> Buku
            </span>
            <span class="mr-2">
              <i class="fas fa-circle text-success"></i> Peminjam
            </span>
            <span class="mr-2">
              <i class="fas fa-circle text-info"></i> Booking
            </span>
            <span class="mr-2">
              <i class="fas fa-circle text-warning"></i> User
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mb-4">
    <div class="card">
        <div class="card-body">
          <h4><?= $website->website_jum; ?></h4>
          <hr>
          <?= $website->website_tentang; ?>
        </div>
      </div>
  </div>
</div>

<script src="<?= base_url('vendor/vendor/chart.js/Chart.min.js'); ?>"></script>
<script>
  // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Peminjaman","Buku","Booking","User"],
    datasets: [{
      data: [ <?= $total_peminjaman; ?>,<?= $total_buku; ?>,<?= $total_booking; ?>,<?= $total_anggota; ?>],
      backgroundColor: ['#1cc88a','#4e73df','#36b9cc','#ffbb00'],
      hoverBackgroundColor: ['#17a673','#2e59d9','#2c9faf','#e68a00'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
});


var klasifikasi = <?php echo json_encode($klasifikasi); ?>;
var jumlahKlasifikasi = <?php echo json_encode($jumlah_klasifikasi) ?>;
var warnaKlasifikasi = [];
var r,g,b;
for (i = 0; i < klasifikasi.length; i++) {
  r = Math.round(Math.random() * 255 + 1);
  g = Math.round(Math.random() * 255 + 1);
  b = Math.round(Math.random() * 255 + 1);
  warnaKlasifikasi.push("rgb("+r+", "+g+", "+b+")");
}
var chartKlasifikasi = document.getElementById("chartKlasifikasi");
var barChart = new Chart(chartKlasifikasi, {
  type: 'bar',
  data: {
    labels: klasifikasi,
    datasets: [{
      data: jumlahKlasifikasi,
      backgroundColor: warnaKlasifikasi,
      label: 'Data klasifikasi'
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      titleFontColor: 'black',
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10
    },
    legend: {
      display: true,
      labels: {
        fontColor: 'black'
      }
    },
    cutoutPercentage: 80,
    scales: {
         yAxes: [{
              ticks: {
                  beginAtZero: true
              }
          }]
      }
  }
});

var perbandinganUser = <?php echo json_encode($perbandingan_user); ?>;
var chartUser = document.getElementById("chartUser");
var barChart = new Chart(chartUser, {
  type: 'bar',
  data: {
    labels: ["Pengunjung", "User"],
    datasets: [{
      data: perbandinganUser,
      backgroundColor: ['#1cc88a','#4e73df'],
      label: 'Jumlah'
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      titleFontColor: 'black',
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10
    },
    legend: {
      display: true,
      labels: {
        fontColor: 'black'
      }
    },
    cutoutPercentage: 80,
    scales: {
         yAxes: [{
              ticks: {
                  beginAtZero: true
              }
          }]
      }
  }
});

var perbandinganBuku = <?php echo json_encode($perbandingan_buku); ?>;
var ctx = document.getElementById("chartBuku");
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Peminjaman","Booking"],
    datasets: [{
      data: perbandinganBuku,
      backgroundColor: ['#1cc88a','#4e73df'],
      hoverBackgroundColor: ['#17a673','#2e59d9'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
});
</script>

