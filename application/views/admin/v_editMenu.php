<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <?= form_open('process_menu_edit'); ?>
            <div class="form-group">
              <label>Menu judul</label>
              <input type="hidden" name="menu_id" value="<?= $menu_item->menu_id; ?>">
              <input type="text" name="menu_judul" class="form-control" value="<?= $menu_item->menu_judul; ?>" required>
              <?= form_error('menu','<small class="text-danger">','</small>') ?>              
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>