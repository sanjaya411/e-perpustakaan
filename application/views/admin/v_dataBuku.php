<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4>Data Buku</h4>
  <div class="card">
    <div class="card-body">
      <a href="<?= base_url("export_buku"); ?>" target="_blank" class="btn btn-primary btn-sm mb-4">Export to PDF</a>
      <table class="table table-bordered table-hover table-responsive" id="data">
        <thead>
          <tr>
            <th width="1%">#</th>
            <th>Judul</th>
            <th>Penulis</th>
            <th>Edisi</th>
            <th>Penerbit</th>
            <th>Tahun Terbit</th>
            <th>Kelas</th>
            <th>Stok</th>
            <th>Tahun Anggaran</th>
            <th>Foto</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach($list_katalog as $item) { ?>
          <tr>
            <td><?= $no++; ?></td>
            <td><?= $item->buku_judul; ?></td>
            <td><?= $item->buku_penulis; ?></td>
            <td><?= $item->buku_edisi; ?></td>
            <td><?= $item->buku_penerbit; ?></td>
            <td><?= $item->buku_tahunTerbit; ?></td>
            <td><?= $item->buku_noSKU; ?></td>
            <td><?= $item->buku_stok; ?></td>
            <td><?= $item->buku_tahunAnggaran; ?></td>
            <td><img src="<?= base_url("vendor/img/buku/".$item->buku_foto) ?>" alt="" style="max-width: 70px;"></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>