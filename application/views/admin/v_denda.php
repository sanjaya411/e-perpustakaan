<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <?= form_open('validation_denda_edit'); ?>
            <div class="form-group">
              <label>Harga Denda</label>
              <input type="number" name="denda" value="<?= $denda->denda_harga; ?>" class="form-control">
              <?= form_error('denda','<small class="text-danger">','<small>'); ?>
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>