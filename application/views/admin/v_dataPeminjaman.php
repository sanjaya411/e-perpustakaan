<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4>Data Peminjaman</h4>
  <div class="card">
    <div class="card-body">
      <a href="" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#add">Tambah data peminjaman</a>
      <a href="<?= base_url('export_peminjaman'); ?>" target="_blank" class="btn btn-info btn-sm mb-3">Export ke Excel</a>
      <!-- <a href="" class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#addEx">Import data Excel</a> -->
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="data">
          <thead>
            <tr>
              <th width="1%">#</th>
              <th>Nomor Peminjaman</th>
              <th>User Peminjam</th>
              <th>Buku</th>
              <th>Jumlah Pinjaman</th>
              <th>Tanggal Meminjam</th>
              <th>Tanggal Pengembalian</th>
              <th>Tanggal Dikembalikan</th>
              <th>Denda</th>
              <th>Status</th>
              <th width="25%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($list_peminjaman as $item) { ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $item->peminjaman_noId; ?></td>
              <td><?= $item->user_nama; ?></td>              
              <td><?= $item->buku_judul; ?></td>
              <td><?= $item->peminjaman_jumlah; ?></td>
              <td><?= date('d M Y', strtotime($item->peminjaman_dari)); ?></td>
              <td><?= date('d M Y', strtotime($item->peminjaman_sampai)); ?></td>
              <td><?php
                if($item->peminjaman_kembali == "0000-00-00") {
                  echo "Masih dipinjam";
                } else {
                  echo date('d M Y', strtotime($item->peminjaman_kembali));
                }
              ?></td>
              <td>Rp. <?= number_format($item->peminjaman_denda,'0',',','.'); ?></td>
              <td>
                <?php
                  if($item->peminjaman_status == 1) {
                    echo '<div class="badge badge-info">Masih dipinjam</div>';
                  } elseif($item->peminjaman_status == 2) {
                    echo '<div class="badge badge-info">Telah dikembalikan</div>';
                  } elseif($item->peminjaman_status == 3) {
                    echo '<div class="badge badge-warning text-dark">Telah dibatalkan</div>';
                  }
                ?>
              </td>
              <td>
                <?php
                  if($item->peminjaman_status == 1) {?>
                    <a href="<?= base_url('peminjaman_dikembalikan/'.$item->peminjaman_id); ?>" class="btn btn-info btn-sm">Dikembalikan</a>
                    <p></p><a href="<?= base_url('peminjaman_batal/'.$item->peminjaman_id); ?>" class="btn btn-warning btn-sm text-dark">Dibatalkan</a>
                    <?php 
                  } 
                  elseif($item->peminjaman_status != 3) { ?>
                    <a href="<?= base_url('peminjaman_hapus/'.$item->peminjaman_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
                    <?php 
                  } ?>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    <h4 class="mt-4">Tabel Denda Harian</h4>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>No</th>
            <th>Harga Denda</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Rp. <?= number_format($list_denda->denda_harga,'0',',','.'); ?></td>
            <td>
              <a href="<?= base_url('denda'); ?>" class="btn btn-info btn-sm">Edit</a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="add">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Tambah Menu</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open('validation_peminjaman_add'); ?>
        <div class="form-group">
          <label>User</label>
          <input type="text" id="searchUser" class="form-control" placeholder="Masukan nama user ..." autocomplete="off">
          <input type="hidden" name="peminjaman_user" id="userId">
          <div class="data-search-user d-none" id="resultUser"></div>
        </div>
        <div class="form-group">
          <label>Buku</label>
          <input type="text" class="form-control" placeholder="Masukan judul buku ..." id="searchBuku" autocomplete="off">
          <input type="hidden" name="peminjaman_buku" id="bukuId">
          <div class="data-buku-peminjaman d-none" id="resultBuku"></div>
        </div>
        <div class="form-group">
          <label>Jumlah Peminjaman</label>
          <input type="number" name="peminjaman_jumlah" class="form-control" value="1">
        </div>
        <div class="form-group">
          <label>Tanggal Peminjaman</label>
          <input type="date" name="peminjaman_dari" class="form-control">
        </div>
        <div class="form-group">
          <label>Tanggal Dikembalikan</label>
          <input type="date" name="peminjaman_sampai" class="form-control">
        </div>
        <input type="submit" value="Simpan" class="btn btn-success btn-sm">
        <?= form_close() ?>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addEx">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Upload file excel</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open_multipart('import_peminjaman') ?>
          <div class="form-group">
            <?= form_label('Unggah file excel'); ?>
            <?= form_upload('files','','class="form-control"'); ?>
          </div>
          <?= form_submit('','Upload','class="btn btn-success btn-sm"') ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
  function addBuku(judulBuku, bukuId) {
      $("#searchBuku").val(judulBuku);
      $("#bukuId").val(bukuId);
    };
  function addUser(userNama, userId) {
    $("#searchUser").val(userNama);
    $("#userId").val(userId);
  };
  $(document).ready(function() {
    $("#searchBuku").on("keyup", function() {
      let searchBuku = $("#searchBuku").val();
      $.ajax({
        url: "<?php echo base_url("search_buku"); ?>",
        type: "POST",
        data: {
          keyword: searchBuku
        },
        cache: false,
        success: function(result) {
          $("#resultBuku").removeClass("d-none");
          $("#resultBuku").html(result);
        }
      });
    })

    $("#searchUser").on("keyup", function() {
      let searchUser = $("#searchUser").val();
      $.ajax({
        url: "<?php echo base_url("search_user"); ?>",
        type: "POST",
        data: {
          keyword: searchUser
        },
        cache: false,
        success: function(result) {
          $("#resultUser").removeClass("d-none");
          $("#resultUser").html(result);
        }
      });
    })

    $("#searchBuku").on("blur", function() {
      window.setTimeout(function() {
        $("#resultBuku").addClass("d-none");
      }, 200)
    })

    $("#searchUser").on("blur", function() {
      window.setTimeout(function() {
        $("#resultUser").addClass("d-none");
      }, 200)
    })
  })
</script>