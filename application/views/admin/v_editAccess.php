<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <?= form_open('validation_access_edit'); ?>
            <div class="form-group">
              <label>Menu judul</label>
              <input type="hidden" name="access_id" value="<?= $access_item->access_id; ?>">
              <select name="menu_id" class="form-control" required>
                <?php foreach($list_menu as $item) { ?>
                  <option <?php if($item->menu_id == $access_item->menu_id ){ echo "selected"; } ?> value="<?= $item->menu_id ?>"><?= $item->menu_judul; ?></option>
                <?php } ?>
              </select>
              <?= form_error('menu','<small class="text-danger">','</small>') ?>              
            </div>
            <div class="form-group">
              <label>Role</label>
              <select name="role_id" class="form-control" required>
                <option <?php if($access_item->role_id == 1 ) { echo 'selected'; } ?> value="1">Admin</option>
                <option <?php if($access_item->role_id == 2 ) { echo 'selected'; } ?> value="2">Petugas</option>
                <option <?php if($access_item->role_id == 3 ) { echo 'selected'; } ?> value="3">User</option>
              </select>
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>