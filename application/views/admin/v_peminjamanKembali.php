<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4>Pengembalian Buku</h4>
  <div class="row"> 
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <?= form_open('validation_peminjaman_kembali'); ?>
            <div class="form-group">
              <label>User</label>
              <select name="peminjaman_user" class="form-control" readonly>
                <?php foreach($list_user as $item) { ?>
                <option <?php if($item->user_id == $peminjaman->peminjaman_user) { echo 'selected'; } ?> value="<?= $item->user_id; ?>"><?= $item->user_nama; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Buku</label>
              <select name="peminjaman_buku" class="form-control" readonly>
                <?php foreach($list_buku as $item) { ?>
                <option <?php if($item->buku_id == $peminjaman->peminjaman_buku) { echo 'selected'; } ?> value="<?= $item->buku_id; ?>"><?= $item->buku_judul; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Jumlah Peminjaman</label>
              <input type="hidden" name="peminjaman_id" value="<?= $peminjaman->peminjaman_id; ?>">
              <input type="number" name="peminjaman_jumlah" class="form-control" value="<?= $peminjaman->peminjaman_jumlah; ?>" readonly>
            </div>
            <div class="form-group">
              <label>Tanggal Peminjaman</label>
              <input type="date" name="peminjaman_dari" class="form-control" value="<?= $peminjaman->peminjaman_dari; ?>" readonly>
            </div>
            <div class="form-group">
              <label>Tanggal Pengembalian</label>
              <input type="date" name="peminjaman_sampai" class="form-control" value="<?= $peminjaman->peminjaman_sampai; ?>" readonly>
            </div>
            <div class="form-group">
              <label>Tanggal Dikembalikan</label>
              <input type="date" name="peminjaman_kembali" class="form-control">
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close() ?>
        </div>
      </div>
    </div>
  </div>
</div>