<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h3>Menu Management</h3>
  <div class="row">
    <div class="col-lg-6">
      <div class="card">
        <div class="card-body">
          <h5 class="text-center">Menu Management</h5>
          <a href="" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#add">Tambah Menu</a>
          <div class="table-responsive">
            <table class="table table-bordered table-hover mb-5" id="data">
              <thead>
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Menu</th>
                  <th class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody id="listMenu">
                <?php $no=1; foreach($list_menu as $item) { ?>
                <tr>
                  <td class="text-center"><?= $no++; ?></td>
                  <td class="text-center"><?= $item->menu_judul; ?></td>
                  <td class="text-center">
                    <a href="<?= base_url('view_menu_edit/'.$item->menu_id); ?>" class="btn btn-info btn-sm">Edit</a>
                    <a href="<?= base_url('process_menu_delete/'.$item->menu_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>          
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="card">
        <div class="card-body">
          <h5 class="text-center">Access Management</h5>
          <a href="" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#addAcc">Tambah Menu Access</a>
          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data1">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>Menu</th>
                  <th>Role</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="listAccess">
                <?php $no=1; foreach($list_access as $item) { ?>
                <tr>
                  <td><?= $no++; ?></td>
                  <td><?= $item->menu_judul; ?></td>
                  <td>
                    <?php
                    if($item->role_id == 1) {
                      echo 'Admin';
                    } elseif($item->role_id == 2) {
                      echo 'Petugas';
                    } elseif($item->role_id == 3) {
                      echo 'User';
                    }
                    ?>
                  </td>
                  <td>
                    <a href="<?= base_url('access_edit/'.$item->access_id); ?>" class="btn btn-info btn-sm">Edit</a>
                    <a href="<?= base_url('access_delete/'.$item->access_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="add">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Tambah Menu</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open('menu'); ?>
          <div class="form-group">
            <?= form_label('Menu') ?>
            <?= form_input("menu_judul", "", "class='form-control' required"); ?>
          </div>
          <input type="submit" value="Simpan" class="btn btn-success btn-sm">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addAcc">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Tambah Access</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open('access_add'); ?>
        <div class="form-group">
          <label>Menu</label>
          <select name="menu_id" class="form-control" required>
            <option disabled selected>-- Pilih Menu --</option>
            <?php foreach($list_menu as $item){ ?>
            <option value="<?= $item->menu_id; ?>"><?= $item->menu_judul; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label>Role</label>
          <select class="form-control" name="role_id" required>
            <option disabled selected>-- Pilih Role --</option>
            <option value="1">Admin</option>
            <option value="2">Petugas</option>
            <option value="3">User</option>
          </select>
        </div>
        <input type="submit" value="Simpan" class="btn btn-success btn-sm">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
      </div>
    </div>
  </div>
</div>