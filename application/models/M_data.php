<?php

use phpDocumentor\Reflection\Types\False_;

defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model {
  protected $response = [];

  //Fungsi Input Daata
  public function insertData($data,$table) {
    $insert = $this->db->insert($table,$data);
    if($insert) {
      $this->response['success'] = TRUE;
    } else {
      $this->response['failed'] = FALSE;
    }
    return $this->response;
  }

  //Fungsi Read Data
  public function getData($table)
  {
    $get = $this->db->get($table);
    if ($get) {
      return $get;
    } else {
      var_dump($get);
      return FALSE;
    }
  }

  //Fungsi Ambil Data tertetnu
  public function editData($where,$table, $limit = "") {
    $get = $this->db->where($where)->get($table, $limit);
    if ($get) {
      return $get;
    } else {
      return FALSE;
    }
  }

  //Fungsi Update Data
  public function updateData($data,$where,$table)
  {
    $update = $this->db->where($where)->update($table,$data);
    if ($update) {
      $this->response['success'] = TRUE;
    } else {
      $this->response['failed'] = FALSE;
    }
    return $this->response;
  }

  //Fungsi hapus data
  public function deleteData($where,$table)
  {
    $delete = $this->db->delete($table,$where);
    if ($delete) {
      $this->response['success'] = TRUE;
    } else {
      $this->response['success'] = FALSE;
    }
    return $this->response;
  }

  //Join Access & Menu
  public function get_access_menu()
  {
    return $this->db->select('*')
                    ->from('tb_menu')
                    ->join('tb_access', 'tb_menu.menu_id = tb_access.menu_id')
                    ->where('tb_access.role_id = '.$this->session->userdata("role_id"))
                    ->get();
  }

  //Delete Menu, Access, Sub Menu
  public function delete_menu($where)
  {
    $delete_sub = $this->db->where($where)->delete("tb_sub");
    if ($delete_sub) {
      $delete_access = $this->db->where($where)->delete("tb_access");
      if ($delete_access) {
        $delete_menu = $this->db->where($where)->delete("tb_menu");
        if ($delete_sub) {
          return TRUE;
        } else {
          return FALSE;
        }
      } else {
        return FALSE;
      }
    } else {
      return False;
    }
  }

  //Join tb_access & tb_menu
  public function get_access_join_menu()
  {
    return $this->db->select('*')
                    ->from('tb_access')
                    ->join('tb_menu', 'tb_menu.menu_id = tb_access.menu_id')
                    ->get();
  }

  //Join tb_sub & tb_menu
  public function get_sub_join_menu()
  {
    return $this->db->select('*')
                    ->from('tb_sub')
                    ->join('tb_menu', 'tb_menu.menu_id = tb_sub.menu_id')
                    ->get();
  }

  //list user dengan role admin & petugas
  public function get_petugas()
  {
    return $this->db->select("*")
                    ->from("tb_user")
                    ->where("user_role = 1")
                    ->or_where("user_role = 2")
                    ->get();
  }

  //Get petugas detail
  public function get_petugas_detail($petugas_id)
  {
    return $this->db->select("*")
                    ->from("`tb_user` tbu")
                    ->join("`tb_identitas_orangtua` tbio", "tbu.`user_id` = tbio.`orangtua_user`")
                    ->join("`tb_pertanyaan_keamanan` tbpk", "tbu.`user_id` = tbpk.`pertanyaan_user`")
                    ->where("tbu.`user_id` = ".$petugas_id)
                    ->get();
  }

  //Get user detail
  public function get_user_detail($user_id)
  {
    return $this->db->select("*")
                    ->from("`tb_user` tbu")
                    ->join("`tb_identitas_orangtua` tbio", "tbu.`user_id` = tbio.`orangtua_user`")
                    ->join("`tb_pertanyaan_keamanan` tbpk", "tbu.`user_id` = tbpk.`pertanyaan_user`")
                    ->where("tbu.`user_id` = ".$user_id)
                    ->get();
  }

  //Get peminjaman join user & Buku
  public function get_peminjaman()
  {
    return $this->db->select("*")
                    ->from("`tb_peminjaman` tbp")
                    ->join("`tb_buku` tbb", "tbp.`peminjaman_buku` = tbb.`buku_id`")
                    ->join("`tb_user` tbu", "tbp.`peminjaman_user` = tbu.`user_id`")
                    ->get();
  }

  public function get_peminjaman_detail($id)
  {
    return $this->db->select("*")
                    ->from("`tb_peminjaman` tbp")
                    ->join("`tb_buku` tbb", "tbp.`peminjaman_buku` = tbb.`buku_id`")
                    ->join("`tb_user` tbu", "tbp.`peminjaman_user` = tbu.`user_id`")
                    ->where("tbp.`peminjaman_id` = ".$id)
                    ->get();
  }

  public function get_booking()
  {
    return $this->db->select('*')
                    ->from('tb_booking')
                    ->join('tb_buku','tb_buku.buku_id=tb_booking.booking_buku')
                    ->join('tb_user','tb_user.user_id=tb_booking.booking_user')
                    ->get();
  }

  public function buku_saya()
  {
    // $this->db->query("SELECT a.*, b.buku_judul FROM tb_booking a LEFT JOIN tb_buku b ON a.booking_buku=b.buku_id ORDER BY a.booking_id DESC");
    return $this->db->select("*")
                    ->from("`tb_booking` tbb ")
                    ->join("`tb_user` tbu", "tbb.`booking_user` = tbu.`user_id`")
                    ->join("`tb_buku` tbbu", "tbb.`booking_buku` = tbbu.`buku_id`")
                    ->where("tbu.`user_id` ", $this->session->userdata("admin_id"))
                    ->get();
  }

  public function booking_saya()
  {
    // $this->db->query("SELECT a.*, b.buku_judul FROM tb_booking a LEFT JOIN tb_buku b ON a.booking_buku=b.buku_id ORDER BY a.booking_id DESC");
    return $this->db->select("*")
                    ->from("`tb_booking` tbb")
                    ->join("`tb_user` tbu", "tbb.`booking_user` = tbu.`user_id`")
                    ->join("`tb_buku` tbbu", "tbb.`booking_buku` = tbbu.`buku_id`")
                    ->where("tbu.`user_id` ", $this->session->userdata("admin_id"))
                    ->where(" buku_judul LIKE '%".$this->input->post('kata')."%' OR  buku_penulis LIKE '%".$this->input->post('kata')."%' OR  buku_penerbit LIKE '%".$this->input->post('kata')."%' OR  buku_tahunTerbit LIKE '%".$this->input->post('kata')."%' ")
                    ->get();
  }  

  public function get_buku_saya()
  {
    return $this->db->select("*")
                    ->from("`tb_booking`")
                    ->join("`tb_user` tbu", "tbb.`booking_user` = tbu.`user_id`")
                    ->join("`tb_buku` tbbu", "tbb.`booking_buku` = tbbu.`buku_id`")
                    ->where(" buku_judul LIKE '%".$this->input->post('kata')."%' OR  buku_penulis LIKE '%".$this->input->post('kata')."%' OR  buku_penerbit LIKE '%".$this->input->post('kata')."%' OR  buku_tahunTerbit LIKE '%".$this->input->post('kata')."%' ORDER BY buku_id DESC ")
                    ->get();
  }  

  public function get_buku()
  {
    return $this->db->select("*")
                    ->from("`tb_buku`")
                    ->where(" buku_judul LIKE '%".$this->input->post('kata')."%' OR  buku_penulis LIKE '%".$this->input->post('kata')."%' OR  buku_penerbit LIKE '%".$this->input->post('kata')."%' OR  buku_tahunTerbit LIKE '%".$this->input->post('kata')."%' ORDER BY buku_id DESC ")
                    ->get();
  }

  public function data($limit,$offset,$where)
  {
    return $this->db->where($where)->get('tb_buku',$limit,$offset);
  }

  public function get_cart_buku()
  {
    return $this->db->select("*")
                    ->from("`tb_cart` tbc")
                    ->join("`tb_buku` tbb", "tbc.`cart_buku` = tbb.`buku_id`")
                    ->where("cart_user", $this->session->userdata("admin_id"))
                    ->get();
  }

  public function get_log()
  {
    return $this->db->select("*")
                    ->from("tb_log")
                    ->join("tb_user", "tb_log.log_user = tb_user.user_id")
                    ->order_by("tb_log.log_id", "DESC")
                    ->get();
  }

  public function search_buku($keyword, $limit)
  {
    $response = new stdClass();
    $query = $this->db->select("*")
                      ->from("tb_buku")
                      ->like("buku_judul", $keyword, "both")
                      ->where("buku_stok >", 0)
                      ->limit($limit)
                      ->get();
    if ($query->num_rows() > 0) {
      $response->success = TRUE;
      $response->data = $query->result();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function search_user($keyword, $limit = FALSE)
  {
    $response = new stdClass();
    $query = $this->db->select("*")
                      ->from("tb_user")
                      ->like("user_nama", $keyword, "both")
                      ->limit($limit)
                      ->get();
    if ($query->num_rows() > 0) {
      $response->success = TRUE;
      $response->data = $query->result();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }
}