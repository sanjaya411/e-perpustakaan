<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Buku extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    protek_login();
    genBooking();
    if ($this->session->userdata("role_id") == 3 ) {
      redirect("block");
    }
  }

  public function view_katalog()
  {
    $data = [
      'title' => 'Katalog Buku',
      'menu' => $this->M_data->get_access_menu()->result_array(),
      'user' => $this->M_data->editData(['user_id' => $this->session->userdata('admin_id')],'tb_user')->row(),
      'list_katalog' => $this->M_data->getData("tb_buku")->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_katalogBuku', $data);
    $this->load->view('template/v_footer');
  }

  public function validation_katalog_add()
  {
    $this->form_validation->set_rules('buku_author', 'Buku', 'required');
    $this->form_validation->set_rules('buku_badanKoorporasi', 'Buku', 'required');
    $this->form_validation->set_rules('buku_seminar', 'Buku', 'required');
    $this->form_validation->set_rules('buku_judulSeragam', 'Buku', 'required');
    $this->form_validation->set_rules('buku_judul', 'Buku', 'required');
    $this->form_validation->set_rules('buku_penulis', 'Buku', 'required');
    $this->form_validation->set_rules('buku_edisi', 'Buku', 'required');
    $this->form_validation->set_rules('buku_kota', 'Buku', 'required');
    $this->form_validation->set_rules('buku_penerbit', 'Buku', 'required');
    $this->form_validation->set_rules('buku_tahunTerbit', 'Buku', 'required|numeric');
    $this->form_validation->set_rules('buku_kolasi', 'Buku', 'required');
    $this->form_validation->set_rules('buku_seri', 'Buku', 'required');
    $this->form_validation->set_rules('buku_judulAsli', 'Buku', 'required');
    $this->form_validation->set_rules('buku_catatan', 'Buku', 'required');
    $this->form_validation->set_rules('buku_blibiografi', 'Buku', 'required');
    $this->form_validation->set_rules('buku_indeks', 'Buku', 'required');
    $this->form_validation->set_rules('buku_isbn', 'Buku', 'required');
    $this->form_validation->set_rules('buku_noSKU', 'Buku', 'required|numeric');
    $this->form_validation->set_rules('buku_stok', 'Buku', 'required|numeric');
    $this->form_validation->set_rules('buku_rak', 'Buku', 'required');
    $this->form_validation->set_rules('buku_sumber1', 'Buku', 'required');
    $this->form_validation->set_rules('buku_keterangan', 'Buku', 'required');
    $this->form_validation->set_rules('buku_tahunAnggaran', 'Buku', 'required|numeric');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal", "Dikarenakan data tidak lengkap/di isi dengan benar!", "error", "tutup")</script>');
      redirect('katalogBuku');
    } else {
      $this->process_katalog_add();
    }
  }

  private function process_katalog_add()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "buku_author" => $input->buku_author,
      "buku_badanKoorporasi" => $input->buku_badanKoorporasi,
      "buku_seminar" => $input->buku_seminar,
      "buku_judulSeragam" => $input->buku_judulSeragam,
      "buku_judul" => $input->buku_judul,
      "buku_penulis" => $input->buku_penulis,
      "buku_edisi" => $input->buku_edisi,
      "buku_kota" => $input->buku_kota,
      "buku_penerbit" => $input->buku_penerbit,
      "buku_tahunTerbit" => $input->buku_tahunTerbit,
      "buku_kolasi" => $input->buku_kolasi,
      "buku_seri" => $input->buku_seri,
      "buku_judulAsli" => $input->buku_judulAsli,
      "buku_catatan" => $input->buku_catatan,
      "buku_blibiografi" => $input->buku_blibiografi,
      "buku_indeks" => $input->buku_indeks,
      "buku_isbn" => $input->buku_isbn,
      "buku_noSKU" => $input->buku_noSKU,
      "buku_stok" => $input->buku_stok,
      "buku_rak" => $input->buku_rak,
      "buku_sumber1" => $input->buku_sumber1,
      "buku_keterangan" => $input->buku_keterangan,
      "buku_tahunAnggaran" => $input->buku_tahunAnggaran,
      "buku_foto" => "default.jpg"
    ];
    $check = $this->M_data->insertData($data, "tb_buku");
    if ($check) {
      $this->session->set_flashdata('pesan', '<script>sweet("Sukses", "Katalog berhasil ditambahkan!", "success", "tutup")</script>');
      redirect('katalogBuku');
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal", "Query failed!", "error", "tutup")</script>');
      redirect('katalogBuku');
    }
  }

  public function process_katalog_delete($id)
  {
    $buku_id = (int)$this->db->escape_str($id);
    $check = $this->M_data->deleteData(["buku_id" => $buku_id], "tb_buku");
    if ($check) {
      $this->session->set_flashdata('pesan', '<script>sweet("Sukses", "Katalog berhasil dihapus!", "success", "tutup")</script>');
      redirect('katalogBuku');
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal", "Query failed!", "error", "tutup")</script>');
      redirect('katalogBuku');
    }
  }

  public function katalog_edit($id)
  {
    $buku_id = (int)$this->db->escape_str($id);
    $check = $this->M_data->editData(["buku_id" => $buku_id], "tb_buku");
    if ($check) {
      $data = [
        'title' => 'Katalog Buku',
        'menu' => $this->M_data->get_access_menu()->result_array(),
        'user' => $this->M_data->editData(['user_id' => $this->session->userdata('admin_id')],'tb_user')->row(),
        'katalog' => $check->row()
      ];
      $this->load->view('template/v_head', $data);
      $this->load->view('admin/v_editKatalog', $data);
      $this->load->view('template/v_footer');
    }
  }

  public function validation_katalog_edit()
  {
    $this->form_validation->set_rules('buku_author', 'Buku', 'required');
    $this->form_validation->set_rules('buku_badanKoorporasi', 'Buku', 'required');
    $this->form_validation->set_rules('buku_seminar', 'Buku', 'required');
    $this->form_validation->set_rules('buku_judulSeragam', 'Buku', 'required');
    $this->form_validation->set_rules('buku_judul', 'Buku', 'required');
    $this->form_validation->set_rules('buku_penulis', 'Buku', 'required');
    $this->form_validation->set_rules('buku_edisi', 'Buku', 'required');
    $this->form_validation->set_rules('buku_kota', 'Buku', 'required');
    $this->form_validation->set_rules('buku_penerbit', 'Buku', 'required');
    $this->form_validation->set_rules('buku_tahunTerbit', 'Buku', 'required|numeric');
    $this->form_validation->set_rules('buku_kolasi', 'Buku', 'required');
    $this->form_validation->set_rules('buku_seri', 'Buku', 'required');
    $this->form_validation->set_rules('buku_judulAsli', 'Buku', 'required');
    $this->form_validation->set_rules('buku_catatan', 'Buku', 'required');
    $this->form_validation->set_rules('buku_blibiografi', 'Buku', 'required');
    $this->form_validation->set_rules('buku_indeks', 'Buku', 'required');
    $this->form_validation->set_rules('buku_isbn', 'Buku', 'required');
    $this->form_validation->set_rules('buku_noSKU', 'Buku', 'required|numeric');
    $this->form_validation->set_rules('buku_stok', 'Buku', 'required|numeric');
    $this->form_validation->set_rules('buku_rak', 'Buku', 'required');
    $this->form_validation->set_rules('buku_sumber1', 'Buku', 'required');
    $this->form_validation->set_rules('buku_keterangan', 'Buku', 'required');
    $this->form_validation->set_rules('buku_tahunAnggaran', 'Buku', 'required|numeric');
    $buku_id = html_escape($this->db->escape_str($this->input->post("buku_id")));
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal", "Dikarenakan data tidak lengkap/di isi dengan benar!", "error", "tutup")</script>');
      redirect('edit_katalog/'.$buku_id);
    } else {
      $this->process_katalog_update();
    }
  }

  private function process_katalog_update()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $buku_foto = $_FILES["buku_foto"]["name"];

    if ($buku_foto != "") {
      $check = $this->M_data->editData(["buku_id" => $input->buku_id], "tb_buku")->row();
      // var_dump($input->petugas_id);
      if ($check->buku_foto != "default.jpg") {
        unlink("./vendor/img/buku/".$check->buku_foto);
      }
      $config['upload_path']          = './vendor/img/buku/';
      $config['allowed_types']        = 'jpg|png|jpeg';
      $config['max_size']             = 5024;
      $config['max_width']            = 2048;
      $config['max_height']           = 1512;

      $this->load->library('upload');
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('buku_foto')) {
        $this->session->set_flashdata('pesan', '<script>sweet("Gagal","Gagal Upload Foto!","error","Tutup")</script>');
        redirect('katalog_edit/'.$input->buku_id);
      }
      $data = ["buku_foto" => $buku_foto];
      $where = ["buku_id" => $input->buku_id];
      $this->M_data->updateData($data, $where, "tb_buku");
    }
    $data = [
      "buku_author" => $input->buku_author,
      "buku_badanKoorporasi" => $input->buku_badanKoorporasi,
      "buku_seminar" => $input->buku_seminar,
      "buku_judulSeragam" => $input->buku_judulSeragam,
      "buku_judul" => $input->buku_judul,
      "buku_penulis" => $input->buku_penulis,
      "buku_edisi" => $input->buku_edisi,
      "buku_kota" => $input->buku_kota,
      "buku_penerbit" => $input->buku_penerbit,
      "buku_tahunTerbit" => $input->buku_tahunTerbit,
      "buku_kolasi" => $input->buku_kolasi,
      "buku_seri" => $input->buku_seri,
      "buku_judulAsli" => $input->buku_judulAsli,
      "buku_catatan" => $input->buku_catatan,
      "buku_blibiografi" => $input->buku_blibiografi,
      "buku_indeks" => $input->buku_indeks,
      "buku_isbn" => $input->buku_isbn,
      "buku_noSKU" => $input->buku_noSKU,
      "buku_stok" => $input->buku_stok,
      "buku_rak" => $input->buku_rak,
      "buku_sumber1" => $input->buku_sumber1,
      "buku_keterangan" => $input->buku_keterangan,
      "buku_tahunAnggaran" => $input->buku_tahunAnggaran
    ];
    $where = ["buku_id" => $input->buku_id];
    $check = $this->M_data->updateData($data, $where, "tb_buku");
    if ($check) {
      $this->session->set_flashdata('pesan', '<script>sweet("Sukses", "Katalog berhasil ditambahkan!", "success", "tutup")</script>');
      redirect('katalogBuku');
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal", "Query failed!", "error", "tutup")</script>');
      redirect('edit_katalog/'.$input->buku_id);
    }
  }

  public function view_buku()
  {
    $data = [
      'title' => 'Data Buku',
      'menu' => $this->M_data->get_access_menu()->result_array(),
      'user' => $this->M_data->editData(['user_id' => $this->session->userdata('admin_id')],'tb_user')->row(),
      'list_katalog' => $this->M_data->getData("tb_buku")->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_dataBuku', $data);
    $this->load->view('template/v_footer');
  }

  public function export_buku()
  {
    $this->load->library('dompdf_gen');
    $data = [
      'title' => 'Cetak Data Buku',
      'list_katalog' => $this->M_data->getData("tb_buku")->result()    
    ];
    $this->load->view('admin/v_pdfBuku', $data);
    
    $uk_kertas = 'A4';
    $orientasi = 'landscape';
    $html = $this->output->get_output();
    $this->dompdf->set_paper($uk_kertas,$orientasi);

    $this->dompdf->load_html($html);
    $this->dompdf->render();
    $this->dompdf->stream('Data_buku.pdf',['Attachment' => 1]);
  }

  public function export_katalog()
  {
    $list_katalog = $this->M_data->getData("tb_buku")->result();

    $spreadsheet = new Spreadsheet;

    $spreadsheet->setActiveSheetIndex(0)
      ->setCellValue('A1', 'Nomor')
      ->setCellValue('B1', 'Author')
      ->setCellValue('C1', 'Badan Koorporasi')
      ->setCellValue('D1', 'Seminar')
      ->setCellValue('E1', 'Judul Seragam')
      ->setCellValue('F1', 'Judul')
      ->setCellValue('G1', 'Penulis')
      ->setCellValue('H1', 'Edisi')
      ->setCellValue('I1', 'Kota')
      ->setCellValue('J1', 'Penerbit')
      ->setCellValue('K1', 'Tahun Terbit')
      ->setCellValue('L1', 'Kolasi')
      ->setCellValue('M1', 'Seri')
      ->setCellValue('N1', 'Judul Asli')
      ->setCellValue('O1', 'Catatan')
      ->setCellValue('P1', 'Blibiografi')
      ->setCellValue('Q1', 'Indeks')
      ->setCellValue('R1', 'ISBN')
      ->setCellValue('S1', 'Kelas')
      ->setCellValue('T1', 'Stok')
      ->setCellValue('U1', 'Rak')
      ->setCellValue('V1', 'Sumber Pertama')
      ->setCellValue('W1', 'Keterangan')
      ->setCellValue('X1', 'Tahun Anggaran');

    $kolom = 2;
    $nomor = 1;
    foreach ($list_katalog as $item) {

      $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A' . $kolom, $nomor)
        ->setCellValue('B' . $kolom, $item->buku_author)
        ->setCellValue('C' . $kolom, $item->buku_badanKoorporasi)
        ->setCellValue('D' . $kolom, $item->buku_seminar)
        ->setCellValue('E' . $kolom, $item->buku_judulSeragam)
        ->setCellValue('F' . $kolom, $item->buku_judul)
        ->setCellValue('G' . $kolom, $item->buku_penulis)
        ->setCellValue('H' . $kolom, $item->buku_edisi)
        ->setCellValue('I' . $kolom, $item->buku_kota)
        ->setCellValue('J' . $kolom, $item->buku_penerbit)
        ->setCellValue('K' . $kolom, $item->buku_tahunTerbit)
        ->setCellValue('L' . $kolom, $item->buku_kolasi)
        ->setCellValue('M' . $kolom, $item->buku_seri)
        ->setCellValue('N' . $kolom, $item->buku_judulAsli)
        ->setCellValue('O' . $kolom, $item->buku_catatan)
        ->setCellValue('P' . $kolom, $item->buku_blibiografi)
        ->setCellValue('Q' . $kolom, $item->buku_indeks)
        ->setCellValue('R' . $kolom, $item->buku_isbn)
        ->setCellValue('S' . $kolom, $item->buku_noSKU)
        ->setCellValue('T' . $kolom, $item->buku_stok)
        ->setCellValue('U' . $kolom, $item->buku_rak)
        ->setCellValue('V' . $kolom, $item->buku_sumber1)
        ->setCellValue('W' . $kolom, $item->buku_keterangan)
        ->setCellValue('X' . $kolom, $item->buku_tahunAnggaran);

      $kolom++;
      $nomor++;
    }

    $writer = new Xlsx($spreadsheet);

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Data_katalog.xlsx"');
    header('Cache-Control: max-age=0');

    $writer->save('php://output');
  }

  public function import_katalog()
  {
    include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

    $config['upload_path'] = realpath('./vendor/file/');
    $config['allowed_types'] = 'xlsx|xls|csv';
    $config['max_size'] = '10000';
    $config['encrypt_name'] = true;

    $this->load->library('upload');
    $this->upload->initialize($config);

    if (!$this->upload->do_upload('import_katalog')) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal upload!","File excel gagal diupload!","error","Tutup")</script>');
      redirect('dataKatalog');
    } else {

      $data_upload = $this->upload->data();

      $excelreader     = new PHPExcel_Reader_Excel2007();
      $loadexcel         = $excelreader->load('./vendor/file/' . $data_upload['file_name']); // Load file yang telah diupload ke folder excel
      $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

      $data = array();

      $numrow = 1;
      foreach ($sheet as $row) {
        if ($numrow > 1) {

          array_push($data, array(
            "buku_author" => $row["B"],
            "buku_badanKoorporasi" => $row["C"],
            "buku_seminar" => $row["D"],
            "buku_judulSeragam" => $row["E"],
            "buku_judul" => $row["F"],
            "buku_penulis" => $row["G"],
            "buku_edisi" => $row["H"],
            "buku_kota" => $row["I"],
            "buku_penerbit" => $row["J"],
            "buku_tahunTerbit" => $row["K"],
            "buku_kolasi" => $row["L"],
            "buku_seri" => $row["M"],
            "buku_judulAsli" => $row["N"],
            "buku_catatan" => $row["O"],
            "buku_blibiografi" => $row["P"],
            "buku_indeks" => $row["Q"],
            "buku_isbn" => $row["R"],
            "buku_noSKU" => $row["S"],
            "buku_stok" => $row["T"],
            "buku_rak" => $row["U"],
            "buku_sumber1" => $row["V"],
            "buku_keterangan" => $row["W"],
            "buku_tahunAnggaran" => $row["X"],
            "buku_foto" => "default.jpg"
          ));

        }
        $numrow++;
      }
      $this->db->insert_batch('tb_buku', $data);
      //delete file from server
      unlink(realpath('./vendor/file/' . $data_upload['file_name']));

      //upload success
      $this->session->set_flashdata('pesan', '<script>sweet("Sukses upload!","File berhasil diupload!","success","Tutup")</script>');
      //redirect halaman
      redirect('katalogBuku');
    }
  }
}