<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class User extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    protek_login();
    genBooking();
    if ($this->session->userdata("role_id") == 3 ) {
      redirect("block");
    }
  }

  public function index()
  {
    $data = [
      'title' => 'Data Anggota',
      'menu' => $this->M_data->get_access_menu()->result_array(),
      'user' => $this->M_data->editData(['user_id' => $this->session->userdata('admin_id')],'tb_user')->row(),
      'list_anggota' => $this->M_data->editData(["user_role" => 3], "tb_user")->result(),
      'list_petugas' => $this->M_data->editData(["user_role" => 2], "tb_user")->result(),
      'list_admin' => $this->M_data->editData(["user_role" => 1], "tb_user")->result(),
      'list_user' => $this->M_data->getData(["tb_user"])->result(),
      'pekerjaan' => $this->M_data->getData("tb_klasifikasi")->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_dataAnggota', $data);
    $this->load->view('template/v_footer');
  }

  public function validation_user_add()
  {
    $this->form_validation->set_rules('user_nama', 'User', 'required');
    $this->form_validation->set_rules('user_tempatLahir', 'User', 'required');
    $this->form_validation->set_rules('user_tanggalLahir', 'User', 'required');
    $this->form_validation->set_rules('user_klasifikasi', 'User', 'required');
    $this->form_validation->set_rules('user_ktp', 'User', 'required|numeric');
    $this->form_validation->set_rules('user_username', 'User', 'required');
    $this->form_validation->set_rules('user_password', 'User', 'required');
    $this->form_validation->set_rules('user_noHP', 'User', 'required|numeric');
    $this->form_validation->set_rules('user_email', 'User', 'required|valid_email|is_unique[tb_user.user_email]');
    $this->form_validation->set_rules('orangtua_nama', 'User', 'required');
    $this->form_validation->set_rules('orangtua_noHP', 'User', 'required|numeric');
    $this->form_validation->set_rules('orangtua_tempatLahir', 'User', 'required');
    $this->form_validation->set_rules('orangtua_tanggalLahir', 'User', 'required');
    $this->form_validation->set_rules('pertanyaan', 'User', 'required');
    $this->form_validation->set_rules('jawaban', 'User', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambah", "Dikarenakan data tidak lengkap/email user sudah terdaftar!", "error", "tutup")</script>');
      redirect("dataAnggota");
    } else {
      $this->process_user_add();
    }
  }

  private function process_user_add()
  {
    $input = (object)$this->db->escape_str($this->input->post());
    $user_id = html_escape($this->input->post('user_noId', true));
    //$user_id = rand(1, 1000000);
    $this->load->library('ciqrcode');

    $config['cacheable']    = true; //boolean, the default is true
    $config['cachedir']     = './vendor/'; //string, the default is application/cache/
    $config['errorlog']     = './vendor/'; //string, the default is application/logs/
    $config['imagedir']     = './vendor/img/qr/'; //direktori penyimpanan qr code
    $config['quality']      = true; //boolean, the default is true
    $config['size']         = '512'; //interger, the default is 1024
    $config['black']        = array(224,255,255); // array, default is array(255,255,255)
    $config['white']        = array(70,130,180); // array, default is array(0,0,0)
    $this->ciqrcode->initialize($config);

		$image_name=$user_id.'.png'; //buat name dari qr code sesuai dengan nim
		
		$dataqr = $user_id;

    $params['data'] = $dataqr; //data yang akan di jadikan QR CODE
    $params['level'] = 'H'; //H=High
    $params['size'] = 10;
    $params['savename'] = $config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
    $this->ciqrcode->generate($params);

    $data1 = [
      'user_noId' => $user_id,
      'user_nama' => $input->user_nama,
      'user_tempatLahir' => $input->user_tempatLahir,
      'user_tanggalLahir' => $input->user_tanggalLahir,
      'user_klasifikasi' => $input->user_klasifikasi,
      'user_ktp' => $input->user_ktp,
      'user_foto' => "default.jpg",
      'user_username' => $input->user_username,
      'user_password' => password_hash($input->user_password, PASSWORD_DEFAULT),
      'user_role' => 3,
      'user_noHP' => $input->user_noHP,
      'user_email' => $input->user_email,
      'user_qr' => $image_name
    ];
    $query = $this->M_data->insertData($data1, "tb_user");
    $user_id = $this->db->insert_id();
    if ($query) {
      $data2 = [
        'orangtua_user' => $user_id,
        'orangtua_nama' => $input->orangtua_nama,
        'orangtua_tempatLahir' => $input->orangtua_tempatLahir,
        'orangtua_tanggalLahir' => $input->orangtua_tanggalLahir,
        'orangtua_noHP' => $input->orangtua_noHP
      ];
      $query2 = $this->M_data->insertData($data2, "tb_identitas_orangtua");
      if ($query2) {
        $data3 = [
          'pertanyaan_user' => $user_id,
          'pertanyaan' => $input->pertanyaan,
          'pertanyaan_jawaban' => $input->jawaban
        ];
        $query3 = $this->M_data->insertData($data3, "tb_pertanyaan_keamanan");
        if ($query3) {
          $this->session->set_flashdata('pesan', '<script>sweet("Sukses menambah", "Data petugas berhasil ditambah!", "success", "tutup")</script>');
          redirect("dataAnggota");
        } else {
          $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambah", "Query3 failed!", "success", "tutup")</script>');
          redirect("dataAnggota");
        }
      } else {
        $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambah", "Query2 failed!", "success", "tutup")</script>');
        redirect("dataAnggota");
      }
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambah", "Query1 failed!", "success", "tutup")</script>');
      redirect("dataAnggota");
    }
  }

  public function view_user_edit($id)
  {
    $user_id = (int)$this->db->escape_str($id);
    $check = $this->M_data->get_user_detail($user_id);
    if ($check) {
      $data = [
        'title' => 'Data Anggota',
        'menu' => $this->M_data->get_access_menu()->result_array(),
        'user' => $this->M_data->editData(['user_id' => $this->session->userdata('admin_id')],'tb_user')->row(),
        'user_detail' => $check->row()
      ];
      $this->load->view('template/v_head', $data);
      $this->load->view('admin/v_editUser', $data);
      $this->load->view('template/v_footer');
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal", "Query failed!", "success", "tutup")</script>');
      redirect("dataAnggota");
    }
  }

  public function validation_user_edit()
  {
    $this->form_validation->set_rules('user_noId', 'User', 'required|is_unique[tb_user.user_noId]');
    $this->form_validation->set_rules('user_nama', 'User', 'required');
    $this->form_validation->set_rules('user_tempatLahir', 'User', 'required');
    $this->form_validation->set_rules('user_tanggalLahir', 'User', 'required');
    $this->form_validation->set_rules('user_klasifikasi', 'User', 'required');
    $this->form_validation->set_rules('user_ktp', 'User', 'required|numeric');
    $this->form_validation->set_rules('user_username', 'User', 'required');
    $this->form_validation->set_rules('user_noHP', 'User', 'required|numeric');
    $this->form_validation->set_rules('user_email', 'User', 'required|valid_email|is_unique[tb_user.user_email]');
    $this->form_validation->set_rules('orangtua_nama', 'User', 'required');
    $this->form_validation->set_rules('orangtua_noHP', 'User', 'required|numeric');
    $this->form_validation->set_rules('orangtua_tempatLahir', 'User', 'required');
    $this->form_validation->set_rules('orangtua_tanggalLahir', 'User', 'required');
    $this->form_validation->set_rules('pertanyaan', 'User', 'required');
    $this->form_validation->set_rules('jawaban', 'User', 'required');
    $user_id = (int)$this->input->post("user_id");
    
    if ($this->form_validation->run() == FALSE) {
      $this->process_user_update();
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal Update", "Isi data dengan benar & lengkap!", "success", "tutup")</script>');
      redirect("user_edit/".$user_id);
    }
  }

  private function process_user_update()
  {
    $input = (object)$this->db->escape_str($this->input->post());
    $user_foto = $_FILES["user_foto"]["name"];
    $data = [];
    
    if ($user_foto != "") {
      $check = $this->M_data->editData(["user_id" => $input->petugas_id], "tb_user")->row();
      // var_dump($input->petugas_id);
      if ($check->user_foto != "default.jpg") {
        unlink("./vendor/img/user/".$check->user_foto);
      }
      $config['upload_path']          = './vendor/img/user/';
      $config['allowed_types']        = 'jpg|png|jpeg';
      $config['max_size']             = 802400;
      $config['max_width']            = 100000;
      $config['max_height']           = 100000;

      $this->load->library('upload');
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('user_foto')) {
        $this->session->set_flashdata('pesan', '<script>sweet("Gagal","Gagal Upload Foto!","error","Tutup")</script>');
        redirect('user_edit/'.$input->user_id);
      }
      $data = ["user_foto" => $user_foto];
      $where = ["user_id" => $input->user_id];
      $query = $this->M_data->updateData($data, $where, "tb_user");
    } 
    if ($input->user_password != "") {
      $data = ["user_password" => password_hash($input->user_password, PASSWORD_DEFAULT)];
      $where = ["user_id" => $input->user_id];
      $query = $this->M_data->updateData($data, $where, "tb_user");
    }

    $user_noId=$input->user_noId;

    $this->load->library('ciqrcode');

    $config['cacheable']    = true; //boolean, the default is true
    $config['cachedir']     = './vendor/'; //string, the default is application/cache/
    $config['errorlog']     = './vendor/'; //string, the default is application/logs/
    $config['imagedir']     = './vendor/img/qr/'; //direktori penyimpanan qr code
    $config['quality']      = true; //boolean, the default is true
    $config['size']         = '512'; //interger, the default is 1024
    $config['black']        = array(224,255,255); // array, default is array(255,255,255)
    $config['white']        = array(70,130,180); // array, default is array(0,0,0)
    $this->ciqrcode->initialize($config);

		$image_name=$user_noId.'.png'; //buat name dari qr code sesuai dengan nim
		
		$dataqr = $user_noId;

    $params['data'] = $dataqr; //data yang akan di jadikan QR CODE
    $params['level'] = 'H'; //H=High
    $params['size'] = 10;
    $params['savename'] = $config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
    $this->ciqrcode->generate($params);

    $data = [
      'user_noId' => $input->user_noId,
      'user_nama' => $input->user_nama,
      'user_tempatLahir' => $input->user_tempatLahir,
      'user_tanggalLahir' => $input->user_tanggalLahir,
      'user_klasifikasi' => $input->user_klasifikasi,
      'user_ktp' => $input->user_ktp,
      'user_username' => $input->user_username,
      'user_noHP' => $input->user_noHP,
      'user_email' => $input->user_email,
      'user_qr' => $image_name
    ];
    $where1 = ["user_id" => $input->user_id];
    $query = $this->M_data->updateData($data, $where1, "tb_user");
    if ($query) {
      $data2 = [
        'orangtua_nama' => $input->orangtua_nama,
        'orangtua_tempatLahir' => $input->orangtua_tempatLahir,
        'orangtua_tanggalLahir' => $input->orangtua_tanggalLahir,
        'orangtua_noHP' => $input->orangtua_noHP
      ];
      $where2 = ["orangtua_user" => $input->user_id];
      $query2 = $this->M_data->updateData($data2, $where2, "tb_identitas_orangtua");
      if ($query2) {
        $data3 = [
          'pertanyaan' => $input->pertanyaan,
          'pertanyaan_jawaban' => $input->jawaban
        ];
        $where3 = ["pertanyaan_user" => $input->user_id];
        $query3 = $this->M_data->updateData($data3, $where3, "tb_pertanyaan_keamanan");
        if ($query3) {
          $this->session->set_flashdata('pesan', '<script>sweet("Sukses","Data petugas berhasil diupdate!","success","Tutup")</script>');
          redirect("dataAnggota");
        } else {
          $this->session->set_flashdata('pesan', '<script>sweet("Gagal","Query3 failed!","error","Tutup")</script>');
          redirect('user_edit/'.$input->user_id);
        }
      } else {
        $this->session->set_flashdata('pesan', '<script>sweet("Gagal","Query2 failed!","error","Tutup")</script>');
        redirect('user_edit/'.$input->user_id);
      }
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal","Query1 failed!","error","Tutup")</script>');
      redirect('user_edit/'.$input->user_id);
    }
  }

  public function process_user_delete($id)
  {
    $user_id = (int)$this->db->escape_str($id);
    $get_user = $this->M_data->editData(["user_id" => $user_id], "tb_user")->row();
    unlink("./vendor/img/qr/".$get_user->user_qr);
    $check = $this->M_data->deleteData(["user_id" => $user_id], "tb_user");
    if ($check) {
      $this->M_data->deleteData(["orangtua_user" => $user_id], "tb_identitas_orangtua");
      $this->M_data->deleteData(["pertanyaan_user" => $user_id], "tb_pertanyaan_keamanan");
      $this->session->set_flashdata('pesan', '<script>sweet("Sukses","Data petugas berhasil dihapus!","success","Tutup")</script>');
      redirect("dataAnggota");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal","Query failed!","success","Tutup")</script>');
      redirect("dataAnggota");
    }
  }

  public function cetakUser($id)
  {
    $data = [
      'title' => 'Cetak Kartu Saya',
      'user' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->get_access_menu()->result_array(),
      'user' => $this->M_data->editData(['user_id' => (int)$id],'tb_user')->row()
    ];
    $this->load->view('admin/v_cetakUser',$data);
  }

  public function view_log_user()
  {
    $data = [
      'title' => 'Log User',
      'menu' => $this->M_data->get_access_menu()->result_array(),
      'user' => $this->M_data->editData(['user_id' => $this->session->userdata('admin_id')],'tb_user')->row(),
      'log_user' => $this->M_data->get_log()->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_logUser', $data);
    $this->load->view('template/v_footer');
  }
}