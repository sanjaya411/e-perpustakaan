<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();		
	}

	public function login()
	{
		$this->session->unset_userdata('status');
		$this->session->unset_userdata('admin_id');
		
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[4]',[
			'required' => 'Wajib masukan username!',
			'min_length' => 'Masukan minimal 4 karakter!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[4]',[
			'required' => 'Wajib masukan password!',
			'min_length' => 'Masukan minimal 4 karakter!'
		]);
		$this->form_validation->set_rules('captcha', 'Captcha', 'required',[
			'required' => 'Wajib masukan captcha!'
		]);
		
		
		if ($this->form_validation->run() == FALSE) {
			
			$options = [
				'img_path' => './vendor/img/captcha/',
				'img_url' => base_url('vendor/img/captcha/'),
				'img_width' => 150,
				'img_height' => 30,
				'expiration' => 7200
			];
			$cap = create_captcha($options);
			$img = $cap['image'];
	
			$text = $cap['word'];
			$this->session->set_userdata('captcha',$text);
			
			$data = [
				'img' => $img,
				'text' => $text,
				'title' => 'Login -  Sistem Informasi Perpustakaan'
			];
			$this->load->view('v_login',$data);		
		} else {
			$captcha = $this->input->post('captcha');

			if($captcha == $this->session->userdata('captcha')) {
				$this->prosesLogin();
			} else {
				$this->session->set_flashdata('pesan', '<script>sweet("Gagal Login!","Captcha tidak sesuai!","error","Tutup");</script>');
			
				redirect('login');
			}			
		}		
	}

	private function prosesLogin()
	{
		$user 		= html_escape($this->input->post('username',true));
		$pass 		= html_escape($this->input->post('password',true));

		$cek = $this->M_data->editData(['user_username' => $this->db->escape_str($user)],'tb_user')->row();
		date_default_timezone_set('Asia/Jakarta');

		if($cek) {
			if(password_verify($pass,$cek->user_password)) {
				if($cek->user_role == 1){
					$sesi = [
						'admin_id' => $cek->user_id,
						'role_id' => $cek->user_role,
						'status' => TRUE
					];

					$data = [
						'log_user' => $cek->user_id,
						'log_tanggal' => date('Y-m-d'),
						'log_time' => date('H:i:s')
					];
					$this->M_data->insertData($data,'tb_log');
					
					$this->session->set_userdata($sesi);
					redirect('admin');
				} elseif($cek->user_role == 2){
					$sesi = [
						'admin_id' => $cek->user_id,
						'role_id' => $cek->user_role,
						'status' => TRUE
					];

					$data = [
						'log_user' => $cek->user_id,
						'log_tanggal' => date('Y-m-d'),
						'log_time' => date('H:i:s')
					];
					$this->M_data->insertData($data,'tb_log');
					
					$this->session->set_userdata($sesi);
					redirect('profile');
				} elseif($cek->user_role == 3){
					$sesi = [
						'admin_id' => $cek->user_id,
						'role_id' => $cek->user_role,
						'status' => TRUE
					];

					$data = [
						'log_user' => $cek->user_id,
						'log_tanggal' => date('Y-m-d'),
						'log_time' => date('H:i:s')
					];
					$this->M_data->insertData($data,'tb_log');
					
					$this->session->set_userdata($sesi);
					redirect('index');
				}
			} else {
				$this->session->set_flashdata('pesan', '<script>sweet("Gagal Login!","Password yang anda masukan salah!","error","Tutup");</script>');
			
				redirect('login');
			}
		} else {
			$this->session->set_flashdata('pesan', '<script>sweet("Gagal Login!","Username anda belum terdaftar!","error","Tutup");</script>');
			
			redirect('login');
		}
	}

	public function register()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required',[
			'required' => 'Wajib untuk masukan nama!'
		]);
		$this->form_validation->set_rules('jk', 'jenis kelamin', 'required',[
			'required' => 'Wajib untuk masukan jenis kelamin!'
		]);
		$this->form_validation->set_rules('tempatLahir', 'tempat lahir', 'required',[
			'required' => 'Wajib untuk masukan tempat lahir!'
		]);
		$this->form_validation->set_rules('tanggalLahir', 'tanggal lahir', 'required',[
			'required' => 'Wajib untuk masukan tanggal lahir!'
		]);
		$this->form_validation->set_rules('klasifikasi', 'klasifikasi', 'required',[
			'required' => 'Wajib untuk masukan klasifikasi!'
		]);
		$this->form_validation->set_rules('alamat', 'alamat', 'required',[
			'required' => 'Wajib untuk masukan alamat!'
		]);
		$this->form_validation->set_rules('nomorHP', 'nomor HP', 'required|numeric|min_length[11]|max_length[12]',[
			'required' => 'Wajib untuk masukan nomor HP!',
			'numeric' => 'Masukan nomor HP dengan angka!',
			'min_length' => 'Masukan nomor HP minimal 11 karakter!',
			'max_length' => 'Masukan nomor HP maksimal 12 karakter!'
		]);
		$this->form_validation->set_rules('noKTP', 'nomor induk KTP / Kartu Pelajar', 'required|numeric|is_unique[tb_user.user_ktp]|min_length[10]|max_length[20]',[
			'required' => 'Wajib untuk masukan nomor induk KTP / Kartu Pelajar !',
			'numeric' => 'Masukan nomor induk KTP / Kartu Pelajar hanya angka!',
			'is_unique' => 'Nomor induk KTP / Kartu Pelajar sudah terdaftar!',
			'min_length' => 'Masukan nomor induk KTP / Kartu Pelajar minimal 10 karakter!',
			'max_length' => 'Masukan nomor induk KTP / Kartu Pelajar maksimal 20 karakter!'
		]);
		$this->form_validation->set_rules('mail', 'email', 'required|valid_email|is_unique[tb_user.user_email]',[
			'required' => 'Wajib untuk masukan email!',
			'valid_email' => 'Masukan format email dengan benar!',
			'is_unique' => 'Email sudah didaftarkan!'
		]);
		$this->form_validation->set_rules('user', 'username', 'required|min_length[4]|is_unique[tb_user.user_username]',[
			'required' => 'Wajib untuk masukan username!',
			'is_unique' => 'Username sudah terdaftar!',
			'min_length' => 'Masukan username minimal 4 karakter!'
		]);
		$this->form_validation->set_rules('pass', 'password', 'required|matches[pas1]|min_length[4]',[
			'required' => 'Wajib untuk masukan password!',
			'matches' => 'Password tidak sesuai!',
			'min_length' => 'Masukan password minimal 4 karakter!'
		]);
		$this->form_validation->set_rules('pas1', 'ulangi password', 'required|matches[pass]|min_length[4]',[
			'required' => 'Wajib untuk masukan ulangi password!',
			'matches' => 'Password tidak sesuai! ulangi password',
			'min_length' => 'Masukan minimal 4 karakter! ulangi password'
		]);
		$this->form_validation->set_rules('namaOrangTua', 'nama orangtua / wali', 'required',[
			'required' => 'Wajib untuk masukan nama orangtua / wali!'
		]);
		$this->form_validation->set_rules('noHPOrangTua', 'nomor HP orangtua', 'required|numeric|min_length[11]|max_length[12]',[
			'required' => 'Wajib untuk masukan nomor HP orangtua / wali!',
			'numeric' => 'Wajib masukan dengan angka!',
			'min_length' => 'Masukan nomor HP minimal 11 karakter!',
			'max_length' => 'Masukan nomor HP maksimal 12 karakter!'
		]);
		$this->form_validation->set_rules('tempatLahirOrangTua', 'tempat lahir orangtua', 'required',[
			'required' => 'Wajib untuk masukan tempat lahir orangtua / wali!'
		]);
		$this->form_validation->set_rules('tanggalLahirOrangTua', 'tanggal lahir orangtua', 'required',[
			'required' => 'Wajib untuk masukan tanggal lahir orangtua / wali!'
		]);
		$this->form_validation->set_rules('alamatOrangTua', 'alamat orangtua', 'required',[
			'required' => 'Wajib untuk masukan alamat orangtua / wali!'
		]);
		$this->form_validation->set_rules('pertanyaan', 'pertanyaan', 'required',[
			'required' => 'Wajib untuk memilih pertanyaan!'
		]);
		$this->form_validation->set_rules('jawaban', 'jawaban', 'required',[
			'required' => 'Wajib untuk menjawab pertanyaan!'
		]);

		
		if($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Register Anggota Perpustakaan',
				"pekerjaan" => $this->M_data->getData("tb_klasifikasi")->result()
			];
			$this->load->view('v_regis',$data);

		} else {
			$this->prosesRegis();
		}		
	}

	private function prosesRegis()
	{
		$nama 					= html_escape($this->input->post('nama', true));
		$jk 					= html_escape($this->input->post('jk', true));
		$tempatLahir 			= html_escape($this->input->post('tempatLahir', true));
		$tanggalLahir 			= html_escape($this->input->post('tanggalLahir', true));
		$klasifikasi 			= html_escape($this->input->post('klasifikasi', true));
		$alamat 				= html_escape($this->input->post('alamat', true));
		$user 					= html_escape($this->input->post('user', true));
		$noKTP 					= html_escape($this->input->post('noKTP', true));
		$nomorHP 				= html_escape($this->input->post('nomorHP', true));
		$mail 					= html_escape($this->input->post('mail', true));
		$pass 					= html_escape($this->input->post('pass', true));
		$namaOrangTua			= html_escape($this->input->post('namaOrangTua', true));
		$alamatOrangTua			= html_escape($this->input->post('alamatOrangTua', true));
		$noHPOrangTua       	= html_escape($this->input->post('noHPOrangTua', true));
		$tempatLahirOrangTua	= html_escape($this->input->post('tempatLahirOrangTua', true));
		$tanggalLahirOrangTua 	= html_escape($this->input->post('tanggalLahirOrangTua', true));
		$pertanyaan 			= html_escape($this->input->post('pertanyaan', true));
		$jawaban 				= html_escape($this->input->post('jawaban', true));
//		$noId 					= rand(1, 1000000);
		$noId 					= html_escape($this->input->post('noId', true));

		$this->load->library('ciqrcode');

		$config['cacheable']    = true; //boolean, the default is true
		$config['cachedir']     = './vendor/'; //string, the default is application/cache/
		$config['errorlog']     = './vendor/'; //string, the default is application/logs/
		$config['imagedir']     = './vendor/img/qr/'; //direktori penyimpanan qr code
		$config['quality']      = true; //boolean, the default is true
		$config['size']         = '512'; //interger, the default is 1024
		$config['black']        = array(224,255,255); // array, default is array(255,255,255)
		$config['white']        = array(70,130,180); // array, default is array(0,0,0)
		$this->ciqrcode->initialize($config);

		$image_name=$noId.'.png'; //buat name dari qr code sesuai dengan nim
		
		$dataqr = $noId;

		$params['data'] = $dataqr; //data yang akan di jadikan QR CODE
		$params['level'] = 'H'; //H=High
		$params['size'] = 10;
		$params['savename'] = $config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		$this->ciqrcode->generate($params);
		
		$tiga=3;
		$data = [
			'user_noId' => $noId,
			'user_nama' => $this->db->escape_str($nama),
			'user_jk' => $this->db->escape_str($jk),
			'user_tempatLahir' => $this->db->escape_str($tempatLahir),
			'user_tanggalLahir' => $this->db->escape_str($tanggalLahir),
			'user_klasifikasi' => $this->db->escape_str($klasifikasi),
			'user_alamat' => $this->db->escape_str($alamat),
			'user_ktp' => $this->db->escape_str($noKTP),
			'user_foto' => 'default.jpg',
			'user_username' => $this->db->escape_str($user),
			'user_password' => password_hash($pass, PASSWORD_DEFAULT),
			'user_role' => $tiga,
			'user_noHP' => $this->db->escape_str($nomorHP),
			'user_email' => $this->db->escape_str($mail),
			'user_qr' => $image_name
		];

		$this->M_data->insertData($data,'tb_user');
		$user_id = $this->M_data->editData(['user_ktp' => $noKTP],'tb_user')->row();

		$data2 = [
			'orangtua_user' => $user_id->user_id,
			'orangtua_nama' => $this->db->escape_str($namaOrangTua),
			'orangtua_alamat' => $this->db->escape_str($alamatOrangTua),
			'orangtua_tempatLahir' => $this->db->escape_str($tempatLahirOrangTua),
			'orangtua_tanggalLahir' => $this->db->escape_str($tanggalLahirOrangTua),
			'orangtua_noHP' => $this->db->escape_str($noHPOrangTua)
		];
		$this->M_data->insertData($data2,'tb_identitas_orangtua');

		$data3 = [
			'pertanyaan_user' => $user_id->user_id,
			'pertanyaan' => $pertanyaan,
			'pertanyaan_jawaban' => $this->db->escape_str($jawaban)
		];
		$this->M_data->insertData($data3,'tb_pertanyaan_keamanan');

		$this->session->set_flashdata('pesan','<script>sweet("Sukses Register","Akun anda berhasil diregistrasi, silahkan login!","success","Tutup");</script>');
		redirect('login');
	}

	public function lupaPassword()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email',[
			'required' => 'Wajib masukan email',
			'valid_email' => 'Masukan format email dengan benar!'
		]);
		
		
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Lupa password'
			];
			$this->load->view('v_lupaPassword',$data);
		} else {
			$email = html_escape($this->input->post('email'));
			
			$this->session->set_userdata('email',$email);
			redirect('pertanyaan');
		}
	}

	public function pertanyaan()
	{
		if(!$this->session->userdata('email')) {
			$this->session->set_flashdata('pesan', '<script>sweet("Gagal!","Masukan email terlebih dahulu!","error","Tutup")</script>');
			redirect('lupaPassword');
		}	else {
			$this->form_validation->set_rules('jawaban', 'Jawaban', 'required',[
				'required' => 'Wajib menjawab pertanyaan!'
			]);
			
			$user_id = $this->M_data->editData(['user_email' => $this->session->userdata('email')],'tb_user')->row();
			if ($this->form_validation->run() == FALSE) {
				$data = [
					'title' => 'Pertanyaan - Lupa Password',
					'pertanyaan' => $this->M_data->editData(['pertanyaan_user' => $user_id->user_id],'tb_pertanyaan_keamanan')->row()
				];
				$this->load->view('v_lupaPassword2',$data);
			} else {
				$jawaban = html_escape($this->input->post('jawaban',true));

				$where = [
					'pertanyaan_user' => $user_id->user_id,
					'pertanyaan_jawaban' => $jawaban
				];
				$cekJawaban = $this->M_data->editData($where,'tb_pertanyaan_keamanan')->row();

				if($cekJawaban) {
					$this->session->set_userdata('user',$user_id->user_id);
					redirect('resetPassword');
				} else {
					$this->session->set_flashdata('pesan', '<script>sweet("Gagal!","Jawaban anda salah!","error","Tutup")</script>');
					redirect('pertanyaan');
				}
			}
		}
	}

	public function resetPassword()
	{
		if(!$this->session->userdata('user')) {
			$this->session->set_flashdata('pesan', '<script>sweet("Gagal!","Wajib menjawab pertanyaan terlebih dahulu!","error","Tutup")</script>');
			redirect('pertanyaan');
		} else {
			$this->form_validation->set_rules('password', 'Password', 'required',[
				'required' => 'Wajib masukan Password'
			]);
			
			if ($this->form_validation->run() == FALSE) {
				$data = ['title' => 'Reset Password'];
				$this->load->view('v_lupaPassword3',$data);
			} else {
				$this->resetPasswordAct();
			}
		}		
	}

	private function resetPasswordAct()
	{
		$password   = html_escape($this->input->post('password',true));
		$data 			= ['user_password' => password_hash($password,PASSWORD_DEFAULT)];
		$where 			= ['user_id' => $this->session->userdata('user')];
		$this->M_data->updateData($data,$where,'tb_user');
		
		$this->session->set_flashdata('pesan', '<script>sweet("Sukses!","Password berhasil diganti!","success","Tutup")</script>');
		redirect('login');
	}

	public function myprofile()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required',[
			'required' => 'Masukan nama!'
		]);
		$this->form_validation->set_rules('hp', 'HP', 'required|numeric',[
			'required' => 'Masukan nomor HP!',
			'numeric' => 'Masukan hanya angka!'
		]);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email',[
			'required' => 'Masukan Email!',
			'valid_email' => 'Masukan format email dengan benar!'
		]);

		
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'w' => $this->M_data->getData('tb_website')->row(),	
				'totalp' => $this->M_data->editData(['cart_user' => $this->session->userdata('id'),'cart_kategori' => 2],'tb_cart')->num_rows(),
				'totalb' => $this->M_data->editData(['cart_user' => $this->session->userdata('id'),'cart_kategori' => 1],'tb_cart')->num_rows(),
				'book' => $this->M_data->editData(['booking_user' => $this->session->userdata('id')],'tb_booking')->num_rows(),
				'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row()
			];
			$this->load->view('v_header',$data);
			$this->load->view('user/v_profile',$data);
			$this->load->view('v_footer');
		} else {
			$this->editProfile();
		}
	}
}
